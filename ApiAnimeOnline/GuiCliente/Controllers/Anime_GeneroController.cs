﻿using GuiCliente.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace GuiCliente.Controllers
{
    public class Anime_GeneroController : Controller
    {
        // GET: Anime_Genero
        public ActionResult Index()
        {
            IEnumerable<GuiAnime_GeneroModel> animeGeneroList;
            HttpResponseMessage response = GlobalVariables.webApiClient.GetAsync("Anime_Genero").Result;
            animeGeneroList = response.Content.ReadAsAsync<IEnumerable<GuiAnime_GeneroModel>>().Result;
            return View(animeGeneroList);
        }

        public ActionResult AddOrEditAnimeGenero(int id = 0)
        {
            if (id == 0)
            {
                return View(new GuiAnime_GeneroModel());
            }
            else
            {
                HttpResponseMessage response = GlobalVariables.webApiClient.GetAsync("Anime_Genero/" + id.ToString()).Result;
                return View(response.Content.ReadAsAsync<GuiAnime_GeneroModel>().Result);
            }
        }

        [HttpPost]
        public ActionResult AddOrEditAnimeGenero(GuiAnime_GeneroModel animeGnero)
        {
            if (animeGnero.idAnimeGenero == 0)
            {
                HttpResponseMessage response = GlobalVariables.webApiClient.PostAsJsonAsync("Anime_Genero", animeGnero).Result;
                TempData["SuccessMessage"] = "Guardado Correctamente";
            }
            else
            {
                HttpResponseMessage response = GlobalVariables.webApiClient.PutAsJsonAsync("Anime_Genero/" + animeGnero.idAnimeGenero, animeGnero).Result;
                TempData["SuccessMessage"] = "Actualizado Correctamente";
            }
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            HttpResponseMessage response = GlobalVariables.webApiClient.DeleteAsync("Anime_Genero/" + id.ToString()).Result;
            TempData["SuccessMessage"] = "Eliminado Correctamente";
            return RedirectToAction("Index");
        }

        public ActionResult ApiTercero()
        {
            return View();
        }
    }
}