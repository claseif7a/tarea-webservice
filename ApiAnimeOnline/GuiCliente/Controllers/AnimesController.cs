﻿using GuiCliente.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace GuiCliente.Controllers
{
    public class AnimesController : Controller
    {
        // GET: Animes
        public ActionResult Index()
        {
            IEnumerable<GuiAnimesModel> animesList;
            HttpResponseMessage response = GlobalVariables.webApiClient.GetAsync("Animes").Result;
            animesList = response.Content.ReadAsAsync<IEnumerable<GuiAnimesModel>>().Result;
            return View(animesList);
        }

        public ActionResult AddOrEdit(int id = 0)
        {
            if (id == 0)
            {
                return View(new GuiAnimesModel());
            }
            else
            {
                HttpResponseMessage response = GlobalVariables.webApiClient.GetAsync("Animes/" + id.ToString()).Result;
                return View(response.Content.ReadAsAsync<GuiAnimesModel>().Result);
            }
        }

        [HttpPost]
        public ActionResult AddOrEdit(GuiAnimesModel anime)
        {
            if (anime.idAnime == 0)
            {
                HttpResponseMessage response = GlobalVariables.webApiClient.PostAsJsonAsync("Animes", anime).Result;
                TempData["SuccessMessage"] = "Guardado Correctamente";
            }
            else
            {
                HttpResponseMessage response = GlobalVariables.webApiClient.PutAsJsonAsync("Animes/" + anime.idAnime, anime).Result;
                TempData["SuccessMessage"] = "Actualizado Correctamente";
            }
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            HttpResponseMessage response = GlobalVariables.webApiClient.DeleteAsync("Animes/" + id.ToString()).Result;
            TempData["SuccessMessage"] = "Eliminado Correctamente";
            return RedirectToAction("Index");
        }

        public ActionResult ApiTercero()
        {
            return View();
        }
    }
}