﻿using GuiCliente.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace GuiCliente.Controllers
{
    public class GenerosController : Controller
    {
        // GET: Generos
        public ActionResult Index()
        {
            IEnumerable<GuiGenerosModel> generoList;
            HttpResponseMessage response = GlobalVariables.webApiClient.GetAsync("Generos").Result;
            generoList = response.Content.ReadAsAsync<IEnumerable<GuiGenerosModel>>().Result;
            return View(generoList);
        }

        public ActionResult AddOrEditGenero(int id = 0)
        {
            if (id == 0)
            {
                return View(new GuiGenerosModel());
            }
            else
            {
                HttpResponseMessage response = GlobalVariables.webApiClient.GetAsync("Generos/" + id.ToString()).Result;
                return View(response.Content.ReadAsAsync<GuiGenerosModel>().Result);
            }
        }

        [HttpPost]
        public ActionResult AddOrEditGenero(GuiGenerosModel genero)
        {
            if (genero.idGenero == 0)
            {
                HttpResponseMessage response = GlobalVariables.webApiClient.PostAsJsonAsync("Generos", genero).Result;
                TempData["SuccessMessage"] = "Guardado Correctamente";
            }
            else
            {
                HttpResponseMessage response = GlobalVariables.webApiClient.PutAsJsonAsync("Generos/" + genero.idGenero, genero).Result;
                TempData["SuccessMessage"] = "Actualizado Correctamente";
            }
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            HttpResponseMessage response = GlobalVariables.webApiClient.DeleteAsync("Generos/" + id.ToString()).Result;
            TempData["SuccessMessage"] = "Eliminado Correctamente";
            return RedirectToAction("Index");
        }
    }
}