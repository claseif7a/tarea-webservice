﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GuiCliente.Models
{
    public class GuiAnime_GeneroModel
    {
        public int idAnimeGenero { get; set; }

        [Required(ErrorMessage = "Este campo es Requerido")]
        public Nullable<int> idGeneroFK { get; set; }

        [Required(ErrorMessage = "Este campo es Requerido")]
        public Nullable<int> idAnimeFK { get; set; }
    }
}