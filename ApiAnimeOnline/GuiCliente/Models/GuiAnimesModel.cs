﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GuiCliente.Models
{
    public class GuiAnimesModel
    {
        public int idAnime { get; set; }

        [Required(ErrorMessage = "Este campo es Requerido")]
        public string NombreAnime { get; set; }

        [Required(ErrorMessage = "Este campo es Requerido")]
        public string DescripcionAnime { get; set; }

        [Required(ErrorMessage = "Este campo es Requerido")]
        public string Dias { get; set; }

        [Required(ErrorMessage = "Este campo es Requerido")]
        public Nullable<System.TimeSpan> Hora { get; set; }
    }
}