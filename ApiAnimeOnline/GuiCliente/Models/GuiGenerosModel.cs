﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GuiCliente.Models
{
    public class GuiGenerosModel
    {
        public int idGenero { get; set; }

        [Required(ErrorMessage = "Este campo es Requerido")]
        public string NombreGenero { get; set; }

        [Required(ErrorMessage = "Este campo es Requerido")]
        public string DescripcionGenero { get; set; }
    }
}