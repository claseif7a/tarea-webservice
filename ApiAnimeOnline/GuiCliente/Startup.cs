﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(GuiCliente.Startup))]
namespace GuiCliente
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
