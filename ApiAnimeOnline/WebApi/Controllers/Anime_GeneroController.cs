﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebApi.Models;

namespace WebApi.Controllers
{
    public class Anime_GeneroController : ApiController
    {
        private DBModel db = new DBModel();

        // GET: api/Anime_Genero
        public IQueryable<Anime_Genero> GetAnime_Genero()
        {
            return db.Anime_Genero;
        }

        // GET: api/Anime_Genero/5
        [ResponseType(typeof(Anime_Genero))]
        public IHttpActionResult GetAnime_Genero(int id)
        {
            Anime_Genero anime_Genero = db.Anime_Genero.Find(id);
            if (anime_Genero == null)
            {
                return NotFound();
            }

            return Ok(anime_Genero);
        }

        // PUT: api/Anime_Genero/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutAnime_Genero(int id, Anime_Genero anime_Genero)
        {
            if (id != anime_Genero.idAnimeGenero)
            {
                return BadRequest();
            }

            db.Entry(anime_Genero).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!Anime_GeneroExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Anime_Genero
        [ResponseType(typeof(Anime_Genero))]
        public IHttpActionResult PostAnime_Genero(Anime_Genero anime_Genero)
        {
            db.Anime_Genero.Add(anime_Genero);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = anime_Genero.idAnimeGenero }, anime_Genero);
        }

        // DELETE: api/Anime_Genero/5
        [ResponseType(typeof(Anime_Genero))]
        public IHttpActionResult DeleteAnime_Genero(int id)
        {
            Anime_Genero anime_Genero = db.Anime_Genero.Find(id);
            if (anime_Genero == null)
            {
                return NotFound();
            }

            db.Anime_Genero.Remove(anime_Genero);
            db.SaveChanges();

            return Ok(anime_Genero);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool Anime_GeneroExists(int id)
        {
            return db.Anime_Genero.Count(e => e.idAnimeGenero == id) > 0;
        }
    }
}