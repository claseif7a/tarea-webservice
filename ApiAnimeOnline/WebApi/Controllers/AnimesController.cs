﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebApi.Models;

namespace WebApi.Controllers
{
    public class AnimesController : ApiController
    {
        private DBModel db = new DBModel();

        // GET: api/Animes
        public IQueryable<Anime> GetAnimes()
        {
            return db.Animes;
        }

        // GET: api/Animes/5
        [ResponseType(typeof(Anime))]
        public IHttpActionResult GetAnime(int id)
        {
            Anime anime = db.Animes.Find(id);
            if (anime == null)
            {
                return NotFound();
            }

            return Ok(anime);
        }

        // PUT: api/Animes/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutAnime(int id, Anime anime)
        {
            if (id != anime.idAnime)
            {
                return BadRequest();
            }

            db.Entry(anime).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AnimeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Animes
        [ResponseType(typeof(Anime))]
        public IHttpActionResult PostAnime(Anime anime)
        {
            db.Animes.Add(anime);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = anime.idAnime }, anime);
        }

        // DELETE: api/Animes/5
        [ResponseType(typeof(Anime))]
        public IHttpActionResult DeleteAnime(int id)
        {
            Anime anime = db.Animes.Find(id);
            if (anime == null)
            {
                return NotFound();
            }

            db.Animes.Remove(anime);
            db.SaveChanges();

            return Ok(anime);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool AnimeExists(int id)
        {
            return db.Animes.Count(e => e.idAnime == id) > 0;
        }
    }
}