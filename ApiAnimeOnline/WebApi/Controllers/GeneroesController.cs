﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebApi.Models;

namespace WebApi.Controllers
{
    public class GenerosController : ApiController
    {
        private DBModel db = new DBModel();

        // GET: api/Generoes
        public IQueryable<Genero> GetGeneros()
        {
            return db.Generos;
        }

        // GET: api/Generoes/5
        [ResponseType(typeof(Genero))]
        public IHttpActionResult GetGenero(int id)
        {
            Genero genero = db.Generos.Find(id);
            if (genero == null)
            {
                return NotFound();
            }

            return Ok(genero);
        }

        // PUT: api/Generoes/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutGenero(int id, Genero genero)
        {
            if (id != genero.idGenero)
            {
                return BadRequest();
            }

            db.Entry(genero).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!GeneroExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Generoes
        [ResponseType(typeof(Genero))]
        public IHttpActionResult PostGenero(Genero genero)
        {
            db.Generos.Add(genero);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = genero.idGenero }, genero);
        }

        // DELETE: api/Generoes/5
        [ResponseType(typeof(Genero))]
        public IHttpActionResult DeleteGenero(int id)
        {
            Genero genero = db.Generos.Find(id);
            if (genero == null)
            {
                return NotFound();
            }

            db.Generos.Remove(genero);
            db.SaveChanges();

            return Ok(genero);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool GeneroExists(int id)
        {
            return db.Generos.Count(e => e.idGenero == id) > 0;
        }
    }
}