CREATE DATABASE AnimeOnline
USE AnimeOnline
ALTER DATABASE AnimeOnline COLLATE Latin1_General_CS_AS --Cambiando de Collate
Set dateformat dmy;	

CREATE TABLE Generos
(
 idGenero int identity,
 NombreGenero varchar(30),
 DescripcionGenero nvarchar(max),
 PRIMARY KEY(idGenero)
);
INSERT INTO Generos VALUES	('Shounen','Dirigida especialmente a varones j�venes.'),
							('Acci�n', 'Momentos divetidos, llenos de adrenalina');
SELECT * FROM Generos

CREATE TABLE Animes
(
 idAnime int identity,
 NombreAnime varchar(30),
 DescripcionAnime nvarchar(max),
 Dias varchar(60),
 Hora time(0),
 PRIMARY KEY(idAnime),
);
INSERT INTO Animes VALUES('Dragon Ball Super','Una nueva Era aparece? la de los dioses','Sabado','19:00');
SELECT * FROM Animes

CREATE TABLE Anime_Genero
(
 idAnimeGenero int identity,
 idGeneroFK int,
 idAnimeFK int,
 PRIMARY KEY(idAnimeGenero),
 FOREIGN KEY(idAnimeFK) REFERENCES Animes(idAnime),
 FOREIGN KEY(idGeneroFK) REFERENCES Generos(idGenero)
);
INSERT INTO Anime_Genero VALUES(1,1),(2,1),(1003,2);
s
--Consultas
SELECT * FROM Animes
SELECT * FROM Generos
SELECT * FROM Anime_Genero

SELECT A.NombreAnime, A.DescripcionAnime, A.Dias, A.Hora, G.NombreGenero FROM Animes AS A INNER JOIN Anime_Genero AS AG
ON A.idAnime = AG.idAnimeFK JOIN Generos as G
ON G.idGenero = AG.idGeneroFK

SELECT G.NombreGenero FROM Anime_Genero AS AG INNER JOIN Animes AS A
ON A.idAnime = AG.idAnimeFK JOIN Generos as G
ON G.idGenero = AG.idGeneroFK
