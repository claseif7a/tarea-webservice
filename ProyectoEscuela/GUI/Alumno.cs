﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace GUI
{
    public partial class Alumno : Form
    {
        public Alumno()
        {
            InitializeComponent();
        }

        private void BtnAgregar_Click(object sender, EventArgs e)
        {
            int matricula;
            localhost.WebServices web = new localhost.WebServices();
            web.insertAlumnos(TxtNombre.Text.Trim(), TxtApellidoPaterno.Text.Trim(), TxtApellidoMaterno.Text.Trim(), matricula =int.Parse(TxtMatriculo.Text.Trim()));

            
            DialogResult mensaje = MessageBox.Show("Elemento insertardo"," titulo", MessageBoxButtons.OK);
            if (mensaje == DialogResult.OK)
            {
                DgwAlumnos.DataSource = Mostrar();
            }
            //DataTable dt = new DataTable();
            //DataRow dr;
            ////dt.Columns.Add(new DataColumn("ID_Alumno", System.Type.GetType("System.String")));
            //dt.Columns.Add(new DataColumn("NombreA", System.Type.GetType("System.String")));
            //dt.Columns.Add(new DataColumn("ApellidoPaternoA", System.Type.GetType("System.String")));
            //dt.Columns.Add(new DataColumn("ApellidoMaternoA", System.Type.GetType("System.String")));
            //dt.Columns.Add(new DataColumn("Matricula", System.Type.GetType("System.String")));
            //dr = dt.NewRow();
            ////dr["ID_Alumno"] = this.TxtNombre.Text
            //dr["NombreA"] = this.TxtNombre.Text;
            //dr["ApellidoPaternoA"] = this.TxtApellidoPaterno.Text;
            //dr["ApellidoMaternoA"] = this.TxtApellidoMaterno.Text;
            //dr["Matricula"] = this.TxtMatriculo.Text;
            //dt.Rows.Add(dr);
            //DgwAlumnos.DataSource = dt;
        }

        private DataTable Mostrar()
        {
            localhost.WebServices web = new localhost.WebServices();
            DataTable dt = new DataTable();
            using (SqlConnection cnn = new SqlConnection("Data Source=(local); Initial Catalog=ProyectoEscuela;Integrated Security=True"))
            {
                cnn.Open();
                string mostrando = "select * from Alumnos";
                SqlCommand cmd = new SqlCommand(mostrando,cnn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }
            return dt;
        }
        private void button1_Click(object sender, EventArgs e)
        {
            DgwAlumnos.DataSource = Mostrar();
        }

        private void BtnActualizar_Click(object sender, EventArgs e)
        {
            int matricula;
            localhost.WebServices web = new localhost.WebServices();
            web.actualizarAlumno(TxtNombre.Text.Trim(), TxtApellidoPaterno.Text.Trim(), TxtApellidoMaterno.Text.Trim(), matricula = int.Parse(TxtMatriculo.Text.Trim()));
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            int matricula = Convert.ToInt32(TxtMatriculo.Text);
            localhost1.WebServices web = new localhost1.WebServices();
            web.eliminarAlumnos(matricula);
            

        }
    }
}
