﻿namespace GUI
{
    partial class Inicio
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnAlumno = new System.Windows.Forms.Button();
            this.BtnMaestro = new System.Windows.Forms.Button();
            this.BtnMateria = new System.Windows.Forms.Button();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.BtnExit = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnAlumno
            // 
            this.btnAlumno.Location = new System.Drawing.Point(0, 12);
            this.btnAlumno.Name = "btnAlumno";
            this.btnAlumno.Size = new System.Drawing.Size(97, 77);
            this.btnAlumno.TabIndex = 0;
            this.btnAlumno.Text = "Alumno";
            this.btnAlumno.UseVisualStyleBackColor = true;
            // 
            // BtnMaestro
            // 
            this.BtnMaestro.Location = new System.Drawing.Point(0, 95);
            this.BtnMaestro.Name = "BtnMaestro";
            this.BtnMaestro.Size = new System.Drawing.Size(97, 77);
            this.BtnMaestro.TabIndex = 1;
            this.BtnMaestro.Text = "Maestro";
            this.BtnMaestro.UseVisualStyleBackColor = true;
            // 
            // BtnMateria
            // 
            this.BtnMateria.Location = new System.Drawing.Point(310, 12);
            this.BtnMateria.Name = "BtnMateria";
            this.BtnMateria.Size = new System.Drawing.Size(97, 77);
            this.BtnMateria.TabIndex = 2;
            this.BtnMateria.Text = "Materia";
            this.BtnMateria.UseVisualStyleBackColor = true;
            // 
            // btnBuscar
            // 
            this.btnBuscar.Location = new System.Drawing.Point(310, 95);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(97, 77);
            this.btnBuscar.TabIndex = 3;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.UseVisualStyleBackColor = true;
            // 
            // BtnExit
            // 
            this.BtnExit.Location = new System.Drawing.Point(155, 95);
            this.BtnExit.Name = "BtnExit";
            this.BtnExit.Size = new System.Drawing.Size(97, 77);
            this.BtnExit.TabIndex = 4;
            this.BtnExit.Text = "Salir";
            this.BtnExit.UseVisualStyleBackColor = true;
            // 
            // Inicio
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(419, 203);
            this.Controls.Add(this.BtnExit);
            this.Controls.Add(this.btnBuscar);
            this.Controls.Add(this.BtnMateria);
            this.Controls.Add(this.BtnMaestro);
            this.Controls.Add(this.btnAlumno);
            this.Name = "Inicio";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnAlumno;
        private System.Windows.Forms.Button BtnMaestro;
        private System.Windows.Forms.Button BtnMateria;
        private System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.Button BtnExit;
    }
}

