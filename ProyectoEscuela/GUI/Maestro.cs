﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUI
{
    public partial class Maestro : Form
    {
        public Maestro()
        {
            InitializeComponent();
        }

        private void BtnAgregar_Click(object sender, EventArgs e)
        {
            
            localhost.WebServices web = new localhost.WebServices();
            web.insertarMaestros(TxtNombre.Text.Trim(), TxtApellidoPaterno.Text.Trim(), TxtApellidoMaterno.Text.Trim());


            DialogResult mensaje = MessageBox.Show("Elemento insertardo", " titulo", MessageBoxButtons.OK);
            if (mensaje == DialogResult.OK)
            {
                DgwAlumnos.DataSource = Mostrar();
            }
        }

        private DataTable Mostrar()
        {
            localhost.WebServices web = new localhost.WebServices();
            DataTable dt = new DataTable();
            using (SqlConnection cnn = new SqlConnection("Data Source=(local); Initial Catalog=ProyectoEscuela;Integrated Security=True"))
            {
                cnn.Open();
                string mostrando = "select * from Alumnos";
                SqlCommand cmd = new SqlCommand(mostrando, cnn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }
            return dt;
        }

        private void BtnActualizar_Click(object sender, EventArgs e)
        {

        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            localhost.WebServices web = new localhost.WebServices();
            
        }
    }
}
