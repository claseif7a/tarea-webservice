CREATE DATABASE ProyectoEscuela
USE ProyectoEscuela

CREATE TABLE Alumnos
(
 ID_Alumno int identity unique not null,
 NombresA varchar(30),
 ApellidoPaternoA varchar(20),
 ApellidoMaternoA varchar(20),
 Matricula int unique,
 Constraint PK_ID_Alumno Primary Key(ID_Alumno)
);

INSERT INTO Alumnos Values('NombreA','ApellidopPaternoA','ApellidoMaternoA',14000);

CREATE TABLE Maestros
(
 ID_Maestro int identity unique not null,
 NombresM varchar(30),
 ApellidoPaternoM varchar(20),
 ApellidoMaternoM varchar(20),
 Constraint PK_ID_Maestro Primary Key(ID_Maestro)

);
INSERT INTO Maestros Values('NombremM','ApellidopPaternoM','ApellidoMaternoM');

CREATE TABLE Materias
(
 ID_Materias int identity unique not null,
 NombreMateria varchar(30),
 Maestro_ID int,
 Constraint PK_ID_Materias Primary Key(ID_Materias),
 Constraint FK_Maestro_ID Foreign Key(Maestro_ID) References Maestros(ID_Maestro)
);
INSERT INTO Materias Values('Matematicas',1);
       
CREATE TABLE Grupos
(
 ID_Grupos int identity unique not null,
 Alumno_ID int,
 Maestros_ID int,
 Materia_ID int,
 Constraint PK_ID_Grupos Primary Key(ID_Grupos),
 Constraint FK_Alumno_ID Foreign Key(Alumno_ID) References Alumnos(ID_Alumno),
 Constraint FK_Maestros_ID Foreign Key(Maestros_ID) References Maestros(ID_Maestro),
 Constraint FK_Materia_ID Foreign Key(Materia_ID) References Materias(ID_Materias)
);