﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Web.Services.Protocols;
using System.Xml.Linq;

namespace ServiceWeb
{
    /// <summary>
    /// Descripción breve de WebServices
    /// </summary>
    [WebService(Namespace = "http://ProyectoWS.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    // [System.Web.Script.Services.ScriptService]
    public class WebServices : System.Web.Services.WebService
    {

        [WebMethod(EnableSession = true)]
        public int insertAlumnos(string inombreA, string iapellidopaternoA, string iapellidomaternoA, int imatriculaA)
        {
            string connStr = ConfigurationManager.ConnectionStrings["coneccionBaseDatos"].ConnectionString;
            SqlConnection con = new SqlConnection(connStr);
            con.Open();
            var cmd = new SqlCommand("Insert into Alumnos(NombresA,ApellidoPaternoA,ApellidoMaternoA,Matricula) values('" + inombreA + "','" + iapellidopaternoA + "','" + iapellidomaternoA + "','" + imatriculaA + "')", con);
            int row = cmd.ExecuteNonQuery();
            return row;
        }

        [WebMethod(EnableSession = true)]
        public int insertarMaestros(string inombreM, string iapellidopaternoM, string iapellidomaternoM)
        {
            string connStr = ConfigurationManager.ConnectionStrings["coneccionBaseDatos"].ConnectionString;
            SqlConnection con = new SqlConnection(connStr);
            con.Open();
            var cmd = new SqlCommand("Insert into Maestros(NombreM,ApellidoPaternoM,ApellidoMaternoM) values('" + inombreM + "','" + iapellidopaternoM + "','" + iapellidomaternoM + "')", con);
            int row = cmd.ExecuteNonQuery();
            return row;
        }

        [WebMethod(EnableSession = true)]
        public int insertarMaterias(int inombreM)
        {
            string connStr = ConfigurationManager.ConnectionStrings["coneccionBaseDatos"].ConnectionString;
            SqlConnection con = new SqlConnection(connStr);
            con.Open();
            var cmd = new SqlCommand("Insert into Materias(NombreMateria) values('" + inombreM + "')", con);
            int row = cmd.ExecuteNonQuery();
            return row;
        }

        [WebMethod(EnableSession = true)]
        public void actualizarAlumno(string anombreA, string aapellidopaternoA, string aapellidomaternoA, int amatriculaA )
        {
            string connStr = ConfigurationManager.ConnectionStrings["coneccionBaseDatos"].ConnectionString;
            SqlConnection con = new SqlConnection(connStr);
            con.Open();

            using (SqlCommand cmd = new SqlCommand("UPDATE Alumnos SET NombresA = @NombresA, ApellidoPaternoA = @ApellidoPaternoA, ApellidoMaternoA = @ApellidoMaternoA WHERE Matricula = @Matricula"))
            {
                cmd.Parameters.AddWithValue("@NombresA", anombreA);
                cmd.Parameters.AddWithValue("@ApellidoPaternoA", aapellidopaternoA);
                cmd.Parameters.AddWithValue("@ApellidoMaternoA", aapellidomaternoA);
                cmd.Parameters.AddWithValue("@Matricula", amatriculaA);
                cmd.Connection = con;
                cmd.ExecuteNonQuery();

            }
        }

        [WebMethod]
        public void eliminarAlumnos(int Matricula)
        {
            string constr = ConfigurationManager.ConnectionStrings["coneccionBaseDatos"].ConnectionString;
            using (SqlConnection con = new SqlConnection(constr))
            {
                using (SqlCommand cmd = new SqlCommand("DELETE FROM Alumnos WHERE Matricula = @Matricula"))
                {
                    cmd.Parameters.AddWithValue("@Matricula", Matricula);
                    cmd.Connection = con;
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }
        }
    }
}
