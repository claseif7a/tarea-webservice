﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Productos
{
    public class Productos
    {
        public string Producto { get; set; }
        public double Cantidad { get; set; }
        public double Precio { get; set; }
        public double Total { get; set; }
    }
}