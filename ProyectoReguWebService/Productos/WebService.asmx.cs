﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace Productos
{
    /// <summary>
    /// Descripción breve de WebService
    /// </summary>
    [WebService(Namespace = "http://Productos.org/", Description ="Podras agregar tus productos al carito", Name ="Carito de compras")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    // [System.Web.Script.Services.ScriptService]
    public class WebService : System.Web.Services.WebService
    {
        public string Respuesta { get; set; }
        

        [WebMethod(EnableSession = true, Description = "Agrega sus productos a su lista")]
        public string AgregarProductos(string _Productos, double _Cantidad, double _Precio)
        {
            List<Productos> ListaProductos;
            if(Session["Bienvenido"] == null)
            {
                ListaProductos = new List<Productos>();
            }
            else
            {
                ListaProductos = (List<Productos>)Session["Bienvenido"];
            }
            //double Total = _Cantidad * _Precio;
            //string CaritoProductos = Productos + " " + Cantidad.ToString() + " " + Precio.ToString() + " " + Total.ToString();
            //ListaProductos.Add(CaritoProductos);
            Productos CaritoProductos = new Productos
            {
                Producto = _Productos,
                Cantidad = _Cantidad,
                Precio = _Precio,
                Total = _Cantidad * _Precio,
            };
            ListaProductos.Add(CaritoProductos);
            Session["Bienvenido"] = ListaProductos;

            return "Total = " + CaritoProductos.Total.ToString();
        }

        [WebMethod(EnableSession = true, Description = "Mira todos sus productos de tu lista")]
        public List<Productos> MisProductos()
        {
            if (Session["Bienvenido"] == null)
            {
                List<Productos> ListaProductos = new List<Productos>();//"No has agregado ningun producto a tu Lista"
                Productos CaritoProductos = new Productos
                {
                    Producto = "No has agregado ningun producto a tu Lista",
                };
                ListaProductos.Add(CaritoProductos);
                return ListaProductos; 
            }
            else
            {
                return(List<Productos>)Session["Bienvenido"];
            }
        }

        [WebMethod(EnableSession = true, Description = "Elimina producto de tu lista")]
        public string EliminarProductos(string _Productos)
        {
            
            List<Productos> ListaProductos;
            if (Session["Bienvenido"] == null)
            {
                ListaProductos = new List<Productos>();
            }
            else
            {
                ListaProductos = (List<Productos>)Session["Bienvenido"];
            }
            
            foreach (var CaritoProductos in ListaProductos)
            {
                
                if (CaritoProductos.Producto == _Productos)
                {
                    ListaProductos.Remove(CaritoProductos);
                    Session["Bienvenido"] = ListaProductos;
                    Respuesta = "Se Elimno con exito";
                    break; 
                }
                else
                {
                    Respuesta = "No se pudo eliminar";
                } 
            }
            return Respuesta;         
        }

        [WebMethod(EnableSession = true, Description = "Mira los productos de tu lista con el precio mayor")]
        public List<Productos> ProductoPrecioMayor(double _precio_mayor)
        {
            if (Session["Bienvenido"] == null)
            {
                List<Productos> ListaProductos = new List<Productos>();//"No has agregado ningun producto a tu Lista"
                Productos CaritoProductos = new Productos
                {
                    Producto = "No has agregado ningun producto a tu Lista",
                };
                ListaProductos.Add(CaritoProductos);
                return ListaProductos;
            }
            else
            {
                List<Productos> ListaProductos = (from Productos in MisProductos()
                                                  where Productos.Precio > _precio_mayor
                                                  select Productos).ToList();
                return ListaProductos;
            }
        }

        [WebMethod(EnableSession = true, Description = "Mira los productos de tu lista con el precio menor")]
        public List<Productos> ProductoPrecioMenor(double _precio_menor)
        {
            if (Session["Bienvenido"] == null)
            {
                List<Productos> ListaProductos = new List<Productos>();//"No has agregado ningun producto a tu Lista"
                Productos CaritoProductos = new Productos
                {
                    Producto = "No has agregado ningun producto a tu Lista",
                };
                ListaProductos.Add(CaritoProductos);
                return ListaProductos;
            }
            else
            {
                List<Productos> ListaProductos = (from Productos in MisProductos()
                                                  where Productos.Precio < _precio_menor
                                                  select Productos).ToList();
                return ListaProductos;
            }
        }

        [WebMethod(EnableSession = true, Description = "Mira los productos Repetidos con cantidad mayor a 1")]
        public List<Productos> ProductosRepetidos()
        {
            if (Session["Bienvenido"] == null)
            {
                List<Productos> ListaProductos = new List<Productos>();//"No has agregado ningun producto a tu Lista"
                Productos CaritoProductos = new Productos
                {
                    Producto = "No has agregado ningun producto a tu Lista",
                };
                ListaProductos.Add(CaritoProductos);
                return ListaProductos;
            }
            else
            { 
                List<Productos> ListaProductos = (from Productos in MisProductos()
                                                  where Productos.Cantidad > 1
                                                  select Productos).ToList();
                return ListaProductos;
            }
        }

        [WebMethod(EnableSession = true, Description = "El total de la compra")]
        public string ProductosTotal()
        {
            if (Session["Bienvenido"] == null)
            {
                return "No has agregado ningun producto a tu Lista";
            }
            else
            {
                List<Productos> ListaProductos = (from Productos in MisProductos()
                                                  select Productos).ToList();
                var PrecioTotalCompra = ListaProductos.Sum(x=> x.Total);
                return "Total de la compra = " + PrecioTotalCompra.ToString();
            }
        }

       
    }
}
