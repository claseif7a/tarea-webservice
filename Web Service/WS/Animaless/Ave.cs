﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WS.Animales
{
    public class Ave
    {
        public string Nombre            { get; set; }
        public string Tamaño            { get; set; }
        public string Clase             { get; set; }
        public string NombreCientífico  { get; set; }
        public string NúmerodeEspecies  { get; set; }
        public string Peso              { get; set; }
        public string Longevidad        { get; set; }
        public string Alimentación      { get; set; }
        public string Dieta             { get; set; }
        public string Reproducción      { get; set; }
        public string Distribución      { get; set; }
        public string PeriodoIncubacion { get; set; }
        public string Origen            { get; set; }


        public Ave[] Aves()
        {
            Ave[] Av = new Ave[]
            {
                new Ave()
                {
                    Nombre = "Águila",
                    Tamaño = "70 – 95 cm",
                    Clase = "Aves",
                    NombreCientífico = "Aquila chrysaetos",
                    NúmerodeEspecies = "1 millón",
                    Peso = "450 gramos – 7 kilogramos",
                    Longevidad = "14 – 25 años",
                    Alimentación = "Carnívora",
                    Dieta = "Carnívora (peces, mamíferos, insectos, reptiles)",
                    Reproducción = "Ovípara",
                    Distribución = "Todo el planeta menos la Antártida",
                    PeriodoIncubacion = "30 – 50 días",
                    Origen = " 150 millones de años"

                },
                new Ave()
                {
                    Nombre = "Pajaro",
                    Tamaño = "6,4 cm- 2,7 metros",
                    Clase = "Aves",
                    NombreCientífico = "Ornithurae",
                    NúmerodeEspecies = "1 millón",
                    Peso = "200 gramos – 40 kilos",
                    Longevidad = "7 – 14 años",
                    Alimentación = "Omnívora",
                    Dieta = "Semilla, insectos",
                    Reproducción = "Ovípara",
                    Distribución = "Todo el planeta menos la Antártida",
                    Origen = "100 millones de años"
                }
            };
            return Av;

        }
    }
}