﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WS.Animales
{
    public class Felino
    {
        public string Nombre { get; set; }
        public string Tamaño { get; set; }
        public string Clase { get; set; }
        public string Orden { get; set; }
        public string NombreCientífico { get; set; }
        public string NúmerodeEspecies { get; set; }
        public string Peso { get; set; }
        public string Longevidad { get; set; }
        public string Alimentación { get; set; }
        public string Dieta { get; set; }
        public string Reproducción { get; set; }
        public string Distribución { get; set; }
        public string PeriodoIncubacion { get; set; }
        public string Origen { get; set; }

        public Felino[] Felinos()
        {
            Felino[] Fel = new Felino[]
            {
                new Felino()
                {
                    Nombre = "Gato",
                    Tamaño = "46 cm (Sin cola)",
                    Clase = "Mammalia",
                    Orden = "Carnivora",
                    NombreCientífico = "Felis catus",
                    NúmerodeEspecies = "100",
                    Peso = "3,6 – 4,5 kg",
                    Longevidad = "12 – 18 años",
                    Alimentación = "Omnívora",
                    Reproducción = "Vivípara",
                    Distribución = "Todo el planeta menos la Antártida",
                    PeriodoIncubacion = "64 – 67 días",
                    Origen = " 12 millones de años"

                },
                new Felino()
                {
                    Nombre = "León",
                    Tamaño = "1,2 metros",
                    Clase = "Mammalia",
                    Orden = "Carnivora",
                    NombreCientífico = "Panthera leo",
                    NúmerodeEspecies = "4",
                    Peso = "120 – 260 kilogramos",
                    Longevidad = "10 – 14 años",
                    Alimentación = "Carnívora",
                    Dieta = "Mamíferos, aves, reptiles, peces",
                    Reproducción = "Vivípara",
                    Distribución = "África, parte de Asia",
                    PeriodoIncubacion = "110 días",
                    Origen = "124 mil años"
                },
                new Felino()
                {
                    Nombre = "Leopardo",
                    Tamaño = "45 – 80 cm",
                    Clase =  "Mammalia",
                    Orden =  "Carnivora",
                    NombreCientífico = "Panthera pardus",
                    NúmerodeEspecies = "27",
                    Peso =  "31 kg",
                    Longevidad = "12 – 17 años",
                    Alimentación = "Carnívora",
                    Dieta =  "Mamíferos, aves",
                    Reproducción = "Vivípara",
                    Distribución = "África y Asia",
                    PeriodoIncubacion = "90 – 105 días",
                },
                new Felino()
                {
                    Nombre = "Lince",
                    Tamaño = "48 – 75 cm",
                    Clase = "Mammalia",
                    Orden = "Carnivora",
                    NombreCientífico = "Lynx",
                    NúmerodeEspecies = "4",
                    Peso = "9 – 18 kg",
                    Longevidad = "13 años",
                    Alimentación = "Carnívora",
                    Dieta = "Pequeños mamíferos, peces, reptiles, aves",
                    Reproducción = "Vivípara",
                    Distribución = "Europa y América",
                    PeriodoIncubacion = "61 – 74 días",
                    Origen = "3,2 millones de años"
                },
                new Felino()
                {
                    Nombre = "Tigre",
                    Tamaño = "70 – 120 cm",
                    Clase =  "Mammalia",
                    Orden = "Carnivora",
                    NombreCientífico = "Panthera tigris",
                    NúmerodeEspecies = "6",
                    Peso = "90 – 310 kg",
                    Longevidad = "20 – 26 años",
                    Alimentación = "Carnívora",
                    Dieta = "Mamíferos, aves, reptiles",
                    Reproducción = "Vivípara",
                    Distribución = "Asia, África",
                    PeriodoIncubacion = "93 – 112 días",
                    Origen = "2 millones de años"
                }
            };
            return Fel;
        }
    }
}