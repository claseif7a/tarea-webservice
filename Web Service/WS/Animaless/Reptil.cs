﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WS.Animales
{
    public class Reptil
    {
        public string Nombre { get; set; }
        public string Tamaño { get; set; }
        public string Orden { get; set; }
        public string Clase { get; set; }
        public string SubClase { get; set; }
        public string NombreCientífico { get; set; }
        public string NúmerodeEspecies { get; set; }
        public string Peso { get; set; }
        public string Longevidad { get; set; }
        public string Alimentación { get; set; }
        public string Dieta { get; set; }
        public string Reproducción { get; set; }
        public string Distribución { get; set; }
        public string PeriodoIncubacion { get; set; }
        public string Origen { get; set; }

        public Reptil[] Reptiles()
        {
            Reptil[] Rep = new Reptil[]
            {
                new Reptil()
                {
                    Nombre = "Cocodrilo",
                    Tamaño = "1,5 – 8,5 metros",
                    Clase = "Sauropsida",
                    Orden = "Crocodilia",
                    NombreCientífico = "Crocodylus niloticus",
                    NúmerodeEspecies = "14",
                    Peso = "500 – 1700 kilogramos",
                    Longevidad = "70 – 100 años",
                    Alimentación = "Carnívora",
                    Dieta = "Peces, mamíferos, reptiles",
                    Reproducción = "Ovípara",
                    Distribución = "Todo el planeta menos la Antártida",
                    PeriodoIncubacion = "90 días",
                    Origen = " 200 millones de años"

                },
                new Reptil()
                {
                    Nombre = "Iguana",
                    Tamaño = "30 – 42 cm",
                    Clase = "Sauropsida",
                    Orden = "Squamata",
                    NombreCientífico = "Iguana",
                    NúmerodeEspecies = "50",
                    Peso = "1,2 – 4 kilogramos",
                    Longevidad = "10 – 20 años",
                    Alimentación = "Omnívora",
                    Dieta = "Frutas, insectos",
                    Reproducción = "Ovípara",
                    Distribución = "América y Europa",
                    PeriodoIncubacion = "60 – 110 días",
                    Origen = "400 millones de años"
                },
                new Reptil()
                {
                    Nombre = "Lagartos",
                    Tamaño = "4 cm – 3 metros",
                    Clase = "Sauropsida",
                    SubClase =  "Diapsida",
                    Orden = "Squamata",
                    NombreCientífico = "Lacertilia",
                    Peso = "10 g – 68 kg",
                    Origen = "200 años",
                    NúmerodeEspecies = "5600",
                    Reproducción =  "Ovípara(y en algunos casos ovovivípara)",
                    Alimentación = "Mayormente carnívora",
                    Distribución = "Todo el planeta excepto la Antártida",

                },
                new Reptil()
                {
                    Nombre = "Serpiente",
                    Tamaño = "10 cm – 6 metros",
                    Clase =  "Sauropsida",
                    Orden =  "Squamata",
                    NombreCientífico = "Serpentes",
                    NúmerodeEspecies = "3500",
                    Peso = "200 gramos – 100 kilogramos",
                    Longevidad = "9 años",
                    Alimentación = "Carnívora",
                    Dieta = "Todo tipo de animales",
                    Reproducción = "Ovípara",
                    Distribución = "Todo el planeta menos la Antártida",
                    PeriodoIncubacion = "2 – 5 meses",
                    Origen = "280 millones de años"
                }
            };
            return Rep;
        }
    }
}