﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web.Script.Serialization;
using WS.Animales;

namespace WS
{
    /// <summary>
    /// Descripción breve de Service
    /// </summary>
    [WebService(Namespace = "http://ProyectoWS.org/", Name ="Animales", Description ="Un web service donde se muestran los tipos de animales")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    // [System.Web.Script.Services.ScriptService]
    public class Service : System.Web.Services.WebService
    {

        //[WebMethod]
        //public string HelloWorld()
        //{
        //    return "Hola a todos";
        //}

        //[WebMethod]
        //public string HoraFecha()
        //{
        //    return DateTime.Now.ToString();
        //}

        //[WebMethod]
        //public string MayorDeEdad(int edad)
        //{
        //    string resultado, respuesta = "10";
        //    try
        //    {

        //        if (edad > 17) { resultado = "Eres mayor de edad " + edad; }
        //        else { resultado = "Eres menor de edad " + edad; }

        //        return resultado + respuesta;
        //    }
        //    catch (Exception ex)
        //    {
        //        return resultado = "Error: " + ex + respuesta;
        //    }

        //}

        [WebMethod]
        public Ave[] Aves()
        {
            Ave Avv = new Ave();

            return Avv.Aves();
        }

        [WebMethod]
        public Felino[] Felinos()
        {
            Felino Fee = new Felino();

            return Fee.Felinos();
        }

        [WebMethod]
        public Reptil[] Reptil()
        {
            Reptil Ree = new Reptil();


            return Ree.Reptiles();
        }
    }
}
