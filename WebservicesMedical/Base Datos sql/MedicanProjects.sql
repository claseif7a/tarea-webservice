CREATE DATABASE ProyectoHospital	--Creando base de datos
Use ProyectoHospital				--Poniendola en uso
ALTER DATABASE ProyectoHospital COLLATE Latin1_General_CS_AS --Cambiando de Collate
Set dateformat dmy;					--Permite entrada multples de fecha

CREATE TABLE TipoUsuario
(
 idTipoUsuario int not null identity,
 Descripcion varchar(30),
 Constraint PK_idTipoUsuario Primary Key(idTipoUsuario),
);

CREATE TABLE Usuario
(
 idUsuario int not null identity,
 usuarioNombre varchar(30) unique,
 contra varchar(50),
 tipoUsuarioFK int, 
 Constraint PK_idUsuario Primary Key(idUsuario),
 Constraint FK_tipoUsuarioFK Foreign Key(tipoUsuarioFK) References TipoUsuario(idTipoUsuario)
);
--Creacion de Tablas
CREATE TABLE Paciente
(
 idPaciente int not null identity,
 numSeguroSocial int unique not null,
 nombresPaciente varchar(20),
 apePatPaciente varchar(15),
 apeMatPaciente varchar(15),
 Constraint PK_idPaciente Primary Key(idPaciente),
 --Constraint FK_
);

CREATE TABLE AsistenteMedica
(
 idAsistenteMedica int not null identity,
 dniAsistenteMedica int not null unique,
 nombresAsistente varchar(20),
 apePatAsistente varchar(15),
 apeMatAsistente varchar(15),
 usuario int,
 Constraint PK_idAsistenteMedica Primary Key(idAsistenteMedica),
 Constraint FK_usuario Foreign Key (usuario) References Usuario(idUsuario) 
);

CREATE TABLE Medico
(
 idMedico int not null identity,
 dniMedico int not null unique,
 nombresMedico varchar(20),
 apePatMedico varchar(15),
 apeMatMedico varchar(15),
 usuario int,
 Constraint PK_idMedico Primary Key(idMedico),
 Constraint FK_UsuarioMe Foreign Key(usuario) References Usuario(idUsuario) 
);

CREATE TABLE Cita
(
 idCita int not null identity,
 idPacienteFK int,
 idAsistenteMedicaFK int,
 idMedicoFK int,
 hora time(0),
 fecha date,
 Prescripcion text,
 Constraint PK_idCita Primary Key(idCita),
 Constraint FK_idPacienteFK Foreign Key(idPacienteFK) References Paciente(idPaciente),
 Constraint FK_idAsistenteMedicaFK Foreign Key(idAsistenteMedicaFK) References AsistenteMedica(idAsistenteMedica),
 Constraint idMedicoFK Foreign Key(idMedicoFK) References Medico(idMedico) 
);
--LLenado de datos

--Tabla Tipo de Usuarios;
INSERT INTO TipoUsuario VALUES
('Administrador'),
('Medico'),
('AsistenteMedico');

--Tabla Usuario
INSERT INTO Usuario VALUES
('Admin','123',1),
('Medico','1234',2),
('Asistente','12345',3);
--Tabla
--table pacientes
INSERT INTO Paciente Values
(14212316,'Martin Erubey','Angulo', 'Sauceda'),
(14212336,'Rocio','Mejia','Manriquez'),
(17181920,'Juan','Lopez','Perez'),
(17181921,'Raul','Alcantar','Rodriguez'),
(17181922,'Rosalinda','Mendoza','Fuentes'),
(17181923,'Roxana','Mendez','Araujo'),
(17181924,'Juan Manuel','Zendejas','Luna'),
(17181925,'Jackeline','Tejeda','Cantu'),
(17181926,'Ariana','Grande','Butera'),
(17181927,'Jordi','Valenzuela','Larez'),
(17181928,'Ariel','Cervantez','Tores');

UPDATE Paciente set apePatPaciente = 'Angulo' Where numSeguroSocial = 17181930

--Tabla AsistenteMedica
INSERT INTO AsistenteMedica VALUES (11223344,'Flor','Campos','Real',3);
Select * From AsistenteMedica
--Tabla Medico
INSERT INTO Medico VALUES (99887766,'Antonio','Herrera','Angulo',2);
Select * From Medico
--Tabla Citas
INSERT INTO Cita VALUES(1,1,1,'10:00','14/11/2017','Prescripción');
Select * From Cita
--Consultas Especiales
SELECT	idCita, (P.nombresPaciente +' '+ P.apePatPaciente +' '+ P.apeMatPaciente)		AS Paciente, 
		(AM.nombresAsistente +' '+ AM.apePatAsistente +' '+ AM.apeMatAsistente) AS Asistente, 
		(M.nombresMedico +' '+ M.apePatMedico +' '+ M.apeMatMedico) AS Medico, 	C.fecha, C.hora
From Cita AS C JOIN Paciente as P 
ON C.idPacienteFK = P.idPaciente JOIN AsistenteMedica as AM
ON C.idAsistenteMedicaFK = AM.idAsistenteMedica JOIN Medico AS M 
ON C.idMedicoFK = M.idMedico
WHERE P.nombresPaciente LIKE 'Rocio' OR P.apePatPaciente LIKE '%Mejia' OR P.apeMatPaciente LIKE '%Manriquez'

SELECT (nombresPaciente +' '+ apePatPaciente +' '+ apeMatPaciente)		AS Paciente
FROM Paciente
	--LOGIN ASISTENTE CUANDO SE LOGin
SELECT AM.idAsistenteMedica, (AM.nombresAsistente +' '+ AM.apePatAsistente +' '+ AM.apeMatAsistente) AS Asistente
FROM Usuario AS U INNER JOIN AsistenteMedica AS AM
ON U.idUsuario = AM.usuario 
WHERE U.usuarioNombre = 'Asistente'
	--Para paciente consultar
SELECT * FROM Paciente WHERE idPaciente = 1;
SELECT * FROM Paciente
 
SELECT GetDate()


