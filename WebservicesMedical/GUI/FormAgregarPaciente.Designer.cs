﻿namespace GUI
{
    partial class FormAgregarPaciente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblApeMaterno = new System.Windows.Forms.Label();
            this.lblApePaterno = new System.Windows.Forms.Label();
            this.lblNombre = new System.Windows.Forms.Label();
            this.lblNss = new System.Windows.Forms.Label();
            this.btnAgregarPaciente = new System.Windows.Forms.Button();
            this.btnCerrar = new System.Windows.Forms.Button();
            this.btnEditar = new System.Windows.Forms.Button();
            this.cbNss = new System.Windows.Forms.ComboBox();
            this.cbNombres = new System.Windows.Forms.ComboBox();
            this.cbApePaterno = new System.Windows.Forms.ComboBox();
            this.cbApeMaterno = new System.Windows.Forms.ComboBox();
            this.txtidPaciente = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lblApeMaterno
            // 
            this.lblApeMaterno.AutoSize = true;
            this.lblApeMaterno.Location = new System.Drawing.Point(9, 154);
            this.lblApeMaterno.Name = "lblApeMaterno";
            this.lblApeMaterno.Size = new System.Drawing.Size(86, 13);
            this.lblApeMaterno.TabIndex = 19;
            this.lblApeMaterno.Text = "Apellido Materno";
            // 
            // lblApePaterno
            // 
            this.lblApePaterno.AutoSize = true;
            this.lblApePaterno.Location = new System.Drawing.Point(9, 106);
            this.lblApePaterno.Name = "lblApePaterno";
            this.lblApePaterno.Size = new System.Drawing.Size(84, 13);
            this.lblApePaterno.TabIndex = 17;
            this.lblApePaterno.Text = "Apellido Paterno";
            // 
            // lblNombre
            // 
            this.lblNombre.AutoSize = true;
            this.lblNombre.Location = new System.Drawing.Point(12, 62);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(49, 13);
            this.lblNombre.TabIndex = 16;
            this.lblNombre.Text = "Nombres";
            // 
            // lblNss
            // 
            this.lblNss.AutoSize = true;
            this.lblNss.Location = new System.Drawing.Point(12, 13);
            this.lblNss.Name = "lblNss";
            this.lblNss.Size = new System.Drawing.Size(93, 13);
            this.lblNss.TabIndex = 15;
            this.lblNss.Text = "No. Seguro Social";
            // 
            // btnAgregarPaciente
            // 
            this.btnAgregarPaciente.Location = new System.Drawing.Point(177, 118);
            this.btnAgregarPaciente.Name = "btnAgregarPaciente";
            this.btnAgregarPaciente.Size = new System.Drawing.Size(108, 27);
            this.btnAgregarPaciente.TabIndex = 11;
            this.btnAgregarPaciente.Text = "Agregar Paciente";
            this.btnAgregarPaciente.UseVisualStyleBackColor = true;
            this.btnAgregarPaciente.Click += new System.EventHandler(this.btnAgregarPaciente_Click);
            // 
            // btnCerrar
            // 
            this.btnCerrar.Location = new System.Drawing.Point(177, 163);
            this.btnCerrar.Name = "btnCerrar";
            this.btnCerrar.Size = new System.Drawing.Size(108, 27);
            this.btnCerrar.TabIndex = 20;
            this.btnCerrar.Text = "Cerrar";
            this.btnCerrar.UseVisualStyleBackColor = true;
            this.btnCerrar.Click += new System.EventHandler(this.btnCerrar_Click);
            // 
            // btnEditar
            // 
            this.btnEditar.Location = new System.Drawing.Point(177, 85);
            this.btnEditar.Name = "btnEditar";
            this.btnEditar.Size = new System.Drawing.Size(108, 27);
            this.btnEditar.TabIndex = 21;
            this.btnEditar.Text = "Editar";
            this.btnEditar.UseVisualStyleBackColor = true;
            this.btnEditar.Click += new System.EventHandler(this.btnEditar_Click);
            // 
            // cbNss
            // 
            this.cbNss.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.cbNss.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple;
            this.cbNss.FormattingEnabled = true;
            this.cbNss.Location = new System.Drawing.Point(12, 29);
            this.cbNss.Name = "cbNss";
            this.cbNss.Size = new System.Drawing.Size(121, 20);
            this.cbNss.TabIndex = 22;
            // 
            // cbNombres
            // 
            this.cbNombres.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.cbNombres.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple;
            this.cbNombres.FormattingEnabled = true;
            this.cbNombres.Location = new System.Drawing.Point(12, 78);
            this.cbNombres.Name = "cbNombres";
            this.cbNombres.Size = new System.Drawing.Size(121, 20);
            this.cbNombres.TabIndex = 23;
            // 
            // cbApePaterno
            // 
            this.cbApePaterno.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.cbApePaterno.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple;
            this.cbApePaterno.FormattingEnabled = true;
            this.cbApePaterno.Location = new System.Drawing.Point(12, 125);
            this.cbApePaterno.Name = "cbApePaterno";
            this.cbApePaterno.Size = new System.Drawing.Size(121, 20);
            this.cbApePaterno.TabIndex = 24;
            // 
            // cbApeMaterno
            // 
            this.cbApeMaterno.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.cbApeMaterno.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple;
            this.cbApeMaterno.FormattingEnabled = true;
            this.cbApeMaterno.Location = new System.Drawing.Point(12, 170);
            this.cbApeMaterno.Name = "cbApeMaterno";
            this.cbApeMaterno.Size = new System.Drawing.Size(121, 20);
            this.cbApeMaterno.TabIndex = 25;
            // 
            // txtidPaciente
            // 
            this.txtidPaciente.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.txtidPaciente.Location = new System.Drawing.Point(247, 10);
            this.txtidPaciente.Name = "txtidPaciente";
            this.txtidPaciente.ReadOnly = true;
            this.txtidPaciente.Size = new System.Drawing.Size(38, 20);
            this.txtidPaciente.TabIndex = 26;
            this.txtidPaciente.Visible = false;
            // 
            // FormAgregarPaciente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.SystemColors.GrayText;
            this.ClientSize = new System.Drawing.Size(297, 206);
            this.ControlBox = false;
            this.Controls.Add(this.txtidPaciente);
            this.Controls.Add(this.cbApeMaterno);
            this.Controls.Add(this.cbApePaterno);
            this.Controls.Add(this.cbNombres);
            this.Controls.Add(this.cbNss);
            this.Controls.Add(this.btnEditar);
            this.Controls.Add(this.btnCerrar);
            this.Controls.Add(this.lblApeMaterno);
            this.Controls.Add(this.lblApePaterno);
            this.Controls.Add(this.lblNombre);
            this.Controls.Add(this.lblNss);
            this.Controls.Add(this.btnAgregarPaciente);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Name = "FormAgregarPaciente";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Agregar Paciente";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblApeMaterno;
        private System.Windows.Forms.Label lblApePaterno;
        private System.Windows.Forms.Label lblNombre;
        private System.Windows.Forms.Label lblNss;
        private System.Windows.Forms.Button btnAgregarPaciente;
        private System.Windows.Forms.Button btnCerrar;
        private System.Windows.Forms.Button btnEditar;
        private System.Windows.Forms.ComboBox cbNss;
        private System.Windows.Forms.ComboBox cbNombres;
        private System.Windows.Forms.ComboBox cbApePaterno;
        private System.Windows.Forms.ComboBox cbApeMaterno;
        private System.Windows.Forms.TextBox txtidPaciente;
    }
}