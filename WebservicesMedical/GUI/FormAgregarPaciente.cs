﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUI
{
    public partial class FormAgregarPaciente : Form
    {
        localhost.WebServices web = new localhost.WebServices();
        public FormAgregarPaciente(string accion, int idPaciente)
        {
            InitializeComponent();
            txtidPaciente.Text = idPaciente.ToString();
            if(accion == "Editar") { btnAgregarPaciente.Visible = false; btnEditar.Visible = true; txtidPaciente.Visible = true; }
            else { btnAgregarPaciente.Visible = true; btnEditar.Visible = false;  }
            Llenarcampos();
        }

        private void btnAgregarPaciente_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro que quieres Agregar al Paciente", "Mensaje", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                if (web.AgregarPaciente(int.Parse(cbNss.Text), cbNombres.Text, cbApePaterno.Text, cbApeMaterno.Text) > 0)
                {
                    MessageBox.Show("Se ha agregado Paciennte Correctamente", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    cbNss.ResetText(); cbNombres.ResetText(); cbApePaterno.ResetText(); cbApeMaterno.ResetText();
                }
                else { MessageBox.Show("Ocurrio un Error vuelva intentarlo", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Error); }
            }
            else
            {
                MessageBox.Show("Operacion cancelada", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro que quieres Actualizar al Paciente", "Mensaje", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                int idpaciente = int.Parse(txtidPaciente.Text);
                if (web.ActualizarPaciente(int.Parse(cbNss.Text), cbNombres.Text, cbApePaterno.Text, cbApeMaterno.Text,idpaciente) > 0)
                {
                    MessageBox.Show("Se ha Actualizado Paciennte Correctamente", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    cbNss.ResetText(); cbNombres.ResetText(); cbApePaterno.ResetText(); cbApeMaterno.ResetText();
                    this.Close();
                }
                else { MessageBox.Show("Ocurrio un Error vuelva intentarlo", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Error); }
            }
            else
            {
                MessageBox.Show("Operacion cancelada", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        
        public void Llenarcampos()
        {
            int idpaciente = int.Parse(txtidPaciente.Text);
            DataTable dt = web.SelecionarPaciente(idpaciente).Tables[0];
            cbNss.DataSource = dt;
            cbNombres.DataSource = dt;
            cbApePaterno.DataSource = dt;
            cbApeMaterno.DataSource = dt;
            cbNss.DisplayMember = "numSeguroSocial";        cbNss.ValueMember = "numSeguroSocial";
            cbNombres.DisplayMember = "nombresPaciente";    cbNombres.ValueMember = "nombresPaciente";
            cbApePaterno.DisplayMember = "apePatPaciente";  cbApePaterno.ValueMember = "apePatPaciente";
            cbApeMaterno.DisplayMember = "apeMatPaciente";  cbApeMaterno.ValueMember = "apeMatPaciente";
        }
           
    }
}
