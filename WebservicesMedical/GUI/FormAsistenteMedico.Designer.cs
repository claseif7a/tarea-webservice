﻿namespace GUI
{
    partial class FormAsistenteMedico
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvCitas = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.btnAgregarPaciente = new System.Windows.Forms.Button();
            this.btnAgndarCita = new System.Windows.Forms.Button();
            this.cbPaciente = new System.Windows.Forms.ComboBox();
            this.cbMedico = new System.Windows.Forms.ComboBox();
            this.lblPaciente = new System.Windows.Forms.Label();
            this.lblMedico = new System.Windows.Forms.Label();
            this.cbAsistente = new System.Windows.Forms.ComboBox();
            this.lblFecha = new System.Windows.Forms.Label();
            this.lblHora = new System.Windows.Forms.Label();
            this.DTP = new System.Windows.Forms.DateTimePicker();
            this.maskedtxtHora = new System.Windows.Forms.MaskedTextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblidCita = new System.Windows.Forms.Label();
            this.txtidCita = new System.Windows.Forms.TextBox();
            this.btnEditarCita = new System.Windows.Forms.Button();
            this.btnEditar = new System.Windows.Forms.Button();
            this.cbidPaciente = new System.Windows.Forms.ComboBox();
            this.cbidMedico = new System.Windows.Forms.ComboBox();
            this.cbidAsistente = new System.Windows.Forms.ComboBox();
            this.lblUsuario = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblBusqueda = new System.Windows.Forms.Label();
            this.lblFiltro = new System.Windows.Forms.Label();
            this.btnReiniciar = new System.Windows.Forms.Button();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.txtBusqueda = new System.Windows.Forms.TextBox();
            this.cbFiltro = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCitas)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvCitas
            // 
            this.dgvCitas.AllowUserToAddRows = false;
            this.dgvCitas.AllowUserToDeleteRows = false;
            this.dgvCitas.AllowUserToOrderColumns = true;
            this.dgvCitas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCitas.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2});
            this.dgvCitas.Location = new System.Drawing.Point(12, 239);
            this.dgvCitas.Name = "dgvCitas";
            this.dgvCitas.ReadOnly = true;
            this.dgvCitas.Size = new System.Drawing.Size(619, 222);
            this.dgvCitas.TabIndex = 0;
            this.dgvCitas.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvCitas_CellContentClick);
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Eliminar";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Text = "Eliminar";
            this.Column1.Width = 49;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Editar";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Text = "Editar";
            this.Column2.Width = 49;
            // 
            // btnAgregarPaciente
            // 
            this.btnAgregarPaciente.Location = new System.Drawing.Point(185, 38);
            this.btnAgregarPaciente.Name = "btnAgregarPaciente";
            this.btnAgregarPaciente.Size = new System.Drawing.Size(102, 24);
            this.btnAgregarPaciente.TabIndex = 1;
            this.btnAgregarPaciente.Text = "Agregar paciente";
            this.btnAgregarPaciente.UseVisualStyleBackColor = true;
            this.btnAgregarPaciente.Click += new System.EventHandler(this.btnAgregarPaciente_Click);
            // 
            // btnAgndarCita
            // 
            this.btnAgndarCita.Location = new System.Drawing.Point(511, 92);
            this.btnAgndarCita.Name = "btnAgndarCita";
            this.btnAgndarCita.Size = new System.Drawing.Size(93, 27);
            this.btnAgndarCita.TabIndex = 2;
            this.btnAgndarCita.Text = "Agendar Cita";
            this.btnAgndarCita.UseVisualStyleBackColor = true;
            this.btnAgndarCita.Click += new System.EventHandler(this.btnAgndarCita_Click);
            // 
            // cbPaciente
            // 
            this.cbPaciente.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbPaciente.FormattingEnabled = true;
            this.cbPaciente.Location = new System.Drawing.Point(13, 38);
            this.cbPaciente.Name = "cbPaciente";
            this.cbPaciente.Size = new System.Drawing.Size(166, 21);
            this.cbPaciente.TabIndex = 3;
            // 
            // cbMedico
            // 
            this.cbMedico.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbMedico.FormattingEnabled = true;
            this.cbMedico.Location = new System.Drawing.Point(13, 82);
            this.cbMedico.Name = "cbMedico";
            this.cbMedico.Size = new System.Drawing.Size(166, 21);
            this.cbMedico.TabIndex = 4;
            // 
            // lblPaciente
            // 
            this.lblPaciente.AutoSize = true;
            this.lblPaciente.Location = new System.Drawing.Point(13, 22);
            this.lblPaciente.Name = "lblPaciente";
            this.lblPaciente.Size = new System.Drawing.Size(49, 13);
            this.lblPaciente.TabIndex = 5;
            this.lblPaciente.Text = "Paciente";
            // 
            // lblMedico
            // 
            this.lblMedico.AutoSize = true;
            this.lblMedico.Location = new System.Drawing.Point(13, 66);
            this.lblMedico.Name = "lblMedico";
            this.lblMedico.Size = new System.Drawing.Size(42, 13);
            this.lblMedico.TabIndex = 6;
            this.lblMedico.Text = "Medico";
            // 
            // cbAsistente
            // 
            this.cbAsistente.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.cbAsistente.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple;
            this.cbAsistente.Enabled = false;
            this.cbAsistente.FormattingEnabled = true;
            this.cbAsistente.Location = new System.Drawing.Point(404, 19);
            this.cbAsistente.Name = "cbAsistente";
            this.cbAsistente.Size = new System.Drawing.Size(166, 21);
            this.cbAsistente.TabIndex = 7;
            // 
            // lblFecha
            // 
            this.lblFecha.AutoSize = true;
            this.lblFecha.Location = new System.Drawing.Point(401, 50);
            this.lblFecha.Name = "lblFecha";
            this.lblFecha.Size = new System.Drawing.Size(37, 13);
            this.lblFecha.TabIndex = 10;
            this.lblFecha.Text = "Fecha";
            // 
            // lblHora
            // 
            this.lblHora.AutoSize = true;
            this.lblHora.Location = new System.Drawing.Point(351, 49);
            this.lblHora.Name = "lblHora";
            this.lblHora.Size = new System.Drawing.Size(30, 13);
            this.lblHora.TabIndex = 11;
            this.lblHora.Text = "Hora";
            // 
            // DTP
            // 
            this.DTP.Location = new System.Drawing.Point(404, 66);
            this.DTP.Name = "DTP";
            this.DTP.Size = new System.Drawing.Size(200, 20);
            this.DTP.TabIndex = 37;
            // 
            // maskedtxtHora
            // 
            this.maskedtxtHora.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.maskedtxtHora.Location = new System.Drawing.Point(354, 65);
            this.maskedtxtHora.Mask = "00:00";
            this.maskedtxtHora.Name = "maskedtxtHora";
            this.maskedtxtHora.Size = new System.Drawing.Size(44, 20);
            this.maskedtxtHora.TabIndex = 38;
            this.maskedtxtHora.ValidatingType = typeof(System.DateTime);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblidCita);
            this.groupBox1.Controls.Add(this.txtidCita);
            this.groupBox1.Controls.Add(this.btnEditarCita);
            this.groupBox1.Controls.Add(this.btnEditar);
            this.groupBox1.Controls.Add(this.btnAgndarCita);
            this.groupBox1.Controls.Add(this.maskedtxtHora);
            this.groupBox1.Controls.Add(this.btnAgregarPaciente);
            this.groupBox1.Controls.Add(this.DTP);
            this.groupBox1.Controls.Add(this.cbPaciente);
            this.groupBox1.Controls.Add(this.cbidPaciente);
            this.groupBox1.Controls.Add(this.lblHora);
            this.groupBox1.Controls.Add(this.cbMedico);
            this.groupBox1.Controls.Add(this.lblFecha);
            this.groupBox1.Controls.Add(this.lblPaciente);
            this.groupBox1.Controls.Add(this.cbAsistente);
            this.groupBox1.Controls.Add(this.lblMedico);
            this.groupBox1.Controls.Add(this.cbidMedico);
            this.groupBox1.Controls.Add(this.cbidAsistente);
            this.groupBox1.Location = new System.Drawing.Point(13, 34);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(618, 129);
            this.groupBox1.TabIndex = 39;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Agregar Cita";
            // 
            // lblidCita
            // 
            this.lblidCita.AutoSize = true;
            this.lblidCita.Enabled = false;
            this.lblidCita.Location = new System.Drawing.Point(266, 80);
            this.lblidCita.Name = "lblidCita";
            this.lblidCita.Size = new System.Drawing.Size(39, 13);
            this.lblidCita.TabIndex = 44;
            this.lblidCita.Text = "ID Cita";
            this.lblidCita.Visible = false;
            // 
            // txtidCita
            // 
            this.txtidCita.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.txtidCita.Location = new System.Drawing.Point(269, 96);
            this.txtidCita.Name = "txtidCita";
            this.txtidCita.ReadOnly = true;
            this.txtidCita.Size = new System.Drawing.Size(76, 20);
            this.txtidCita.TabIndex = 42;
            this.txtidCita.Visible = false;
            // 
            // btnEditarCita
            // 
            this.btnEditarCita.Location = new System.Drawing.Point(404, 92);
            this.btnEditarCita.Name = "btnEditarCita";
            this.btnEditarCita.Size = new System.Drawing.Size(93, 27);
            this.btnEditarCita.TabIndex = 43;
            this.btnEditarCita.Text = "Editar Cita";
            this.btnEditarCita.UseVisualStyleBackColor = true;
            this.btnEditarCita.Visible = false;
            this.btnEditarCita.Click += new System.EventHandler(this.btnEditarCita_Click);
            // 
            // btnEditar
            // 
            this.btnEditar.Location = new System.Drawing.Point(293, 38);
            this.btnEditar.Name = "btnEditar";
            this.btnEditar.Size = new System.Drawing.Size(52, 24);
            this.btnEditar.TabIndex = 42;
            this.btnEditar.Text = "Editar";
            this.btnEditar.UseVisualStyleBackColor = true;
            this.btnEditar.Click += new System.EventHandler(this.btnEditar_Click);
            // 
            // cbidPaciente
            // 
            this.cbidPaciente.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbidPaciente.Enabled = false;
            this.cbidPaciente.FormattingEnabled = true;
            this.cbidPaciente.Location = new System.Drawing.Point(80, 38);
            this.cbidPaciente.Name = "cbidPaciente";
            this.cbidPaciente.Size = new System.Drawing.Size(39, 21);
            this.cbidPaciente.TabIndex = 39;
            // 
            // cbidMedico
            // 
            this.cbidMedico.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbidMedico.Enabled = false;
            this.cbidMedico.FormattingEnabled = true;
            this.cbidMedico.Location = new System.Drawing.Point(80, 82);
            this.cbidMedico.Name = "cbidMedico";
            this.cbidMedico.Size = new System.Drawing.Size(39, 21);
            this.cbidMedico.TabIndex = 40;
            // 
            // cbidAsistente
            // 
            this.cbidAsistente.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbidAsistente.Enabled = false;
            this.cbidAsistente.FormattingEnabled = true;
            this.cbidAsistente.Location = new System.Drawing.Point(531, 19);
            this.cbidAsistente.Name = "cbidAsistente";
            this.cbidAsistente.Size = new System.Drawing.Size(39, 21);
            this.cbidAsistente.TabIndex = 41;
            // 
            // lblUsuario
            // 
            this.lblUsuario.AutoSize = true;
            this.lblUsuario.Location = new System.Drawing.Point(560, 10);
            this.lblUsuario.Name = "lblUsuario";
            this.lblUsuario.Size = new System.Drawing.Size(27, 13);
            this.lblUsuario.TabIndex = 40;
            this.lblUsuario.Text = "user";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lblBusqueda);
            this.groupBox2.Controls.Add(this.lblFiltro);
            this.groupBox2.Controls.Add(this.btnReiniciar);
            this.groupBox2.Controls.Add(this.btnBuscar);
            this.groupBox2.Controls.Add(this.txtBusqueda);
            this.groupBox2.Controls.Add(this.cbFiltro);
            this.groupBox2.Location = new System.Drawing.Point(13, 170);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(618, 63);
            this.groupBox2.TabIndex = 41;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Busquedas";
            // 
            // lblBusqueda
            // 
            this.lblBusqueda.AutoSize = true;
            this.lblBusqueda.Location = new System.Drawing.Point(182, 20);
            this.lblBusqueda.Name = "lblBusqueda";
            this.lblBusqueda.Size = new System.Drawing.Size(55, 13);
            this.lblBusqueda.TabIndex = 47;
            this.lblBusqueda.Text = "Busqueda";
            // 
            // lblFiltro
            // 
            this.lblFiltro.AutoSize = true;
            this.lblFiltro.Location = new System.Drawing.Point(16, 16);
            this.lblFiltro.Name = "lblFiltro";
            this.lblFiltro.Size = new System.Drawing.Size(29, 13);
            this.lblFiltro.TabIndex = 46;
            this.lblFiltro.Text = "Filtro";
            // 
            // btnReiniciar
            // 
            this.btnReiniciar.Location = new System.Drawing.Point(537, 34);
            this.btnReiniciar.Name = "btnReiniciar";
            this.btnReiniciar.Size = new System.Drawing.Size(75, 23);
            this.btnReiniciar.TabIndex = 45;
            this.btnReiniciar.Text = "Reiniciar";
            this.btnReiniciar.UseVisualStyleBackColor = true;
            this.btnReiniciar.Click += new System.EventHandler(this.btnReiniciar_Click);
            // 
            // btnBuscar
            // 
            this.btnBuscar.Location = new System.Drawing.Point(456, 34);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(75, 23);
            this.btnBuscar.TabIndex = 44;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.UseVisualStyleBackColor = true;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // txtBusqueda
            // 
            this.txtBusqueda.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.txtBusqueda.Location = new System.Drawing.Point(185, 36);
            this.txtBusqueda.Name = "txtBusqueda";
            this.txtBusqueda.Size = new System.Drawing.Size(265, 20);
            this.txtBusqueda.TabIndex = 43;
            // 
            // cbFiltro
            // 
            this.cbFiltro.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbFiltro.FormattingEnabled = true;
            this.cbFiltro.Items.AddRange(new object[] {
            "Paciente Apellidos",
            "Medico Apellidos"});
            this.cbFiltro.Location = new System.Drawing.Point(13, 35);
            this.cbFiltro.Name = "cbFiltro";
            this.cbFiltro.Size = new System.Drawing.Size(166, 21);
            this.cbFiltro.TabIndex = 42;
            // 
            // FormAsistenteMedico
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.SystemColors.GrayText;
            this.ClientSize = new System.Drawing.Size(643, 473);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.lblUsuario);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.dgvCitas);
            this.Name = "FormAsistenteMedico";
            this.Text = "FormAsistenteMedico";
            ((System.ComponentModel.ISupportInitialize)(this.dgvCitas)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvCitas;
        private System.Windows.Forms.Button btnAgregarPaciente;
        private System.Windows.Forms.Button btnAgndarCita;
        private System.Windows.Forms.ComboBox cbPaciente;
        private System.Windows.Forms.ComboBox cbMedico;
        private System.Windows.Forms.Label lblPaciente;
        private System.Windows.Forms.Label lblMedico;
        private System.Windows.Forms.ComboBox cbAsistente;
        private System.Windows.Forms.Label lblFecha;
        private System.Windows.Forms.Label lblHora;
        private System.Windows.Forms.DateTimePicker DTP;
        private System.Windows.Forms.MaskedTextBox maskedtxtHora;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblUsuario;
        private System.Windows.Forms.ComboBox cbidAsistente;
        private System.Windows.Forms.ComboBox cbidMedico;
        private System.Windows.Forms.ComboBox cbidPaciente;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.TextBox txtBusqueda;
        private System.Windows.Forms.ComboBox cbFiltro;
        private System.Windows.Forms.Button btnReiniciar;
        private System.Windows.Forms.Label lblBusqueda;
        private System.Windows.Forms.Label lblFiltro;
        private System.Windows.Forms.DataGridViewButtonColumn Column1;
        private System.Windows.Forms.DataGridViewButtonColumn Column2;
        private System.Windows.Forms.Button btnEditar;
        private System.Windows.Forms.TextBox txtidCita;
        private System.Windows.Forms.Button btnEditarCita;
        private System.Windows.Forms.Label lblidCita;
    }
}