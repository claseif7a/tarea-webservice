﻿using System;
using System.Data;
using System.Windows.Forms;

namespace GUI
{
    public partial class FormAsistenteMedico : Form
    {
        localhost.WebServices web = new localhost.WebServices();
        public FormAsistenteMedico(string nombreAsistente)
        {
            InitializeComponent();
            lblUsuario.Text = nombreAsistente;
            CargarData();
            ComboBoxPaciente();
            ComboBoxAsistente();
            ComboBoxMedico();
        }

        private void btnAgregarPaciente_Click(object sender, EventArgs e)
        {
            int idPaciente = Convert.ToInt32(cbidPaciente.Text);
            FormAgregarPaciente paciente = new FormAgregarPaciente(btnAgregarPaciente.Text,idPaciente);
            paciente.ShowDialog(); ComboBoxPaciente();
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            int idPaciente = Convert.ToInt32(cbidPaciente.Text);
            FormAgregarPaciente paciente = new FormAgregarPaciente(btnEditar.Text, idPaciente);
            paciente.ShowDialog(); ComboBoxPaciente();
        }

        private void btnAgndarCita_Click(object sender, EventArgs e)
        {
            int idAsistente = Convert.ToInt32(cbidAsistente.Text),
                idPaciente = Convert.ToInt32(cbidPaciente.Text),
                idMedico = Convert.ToInt32(cbidMedico.Text);
            string Fecha = DTP.Value.ToString("yyyy/MM/dd"), hora = maskedtxtHora.Text;
            string validar = maskedtxtHora.Text; validar = validar.Replace(":","");
            if (validar.Length < 4)
            {
                MessageBox.Show("Favor de llenar el campo Hora", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.None);
            }
            else
            {
                if (MessageBox.Show("Estas seguro que quieres Agregar la Cita", "Mensaje", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    if (web.IngresarCita(idPaciente, idAsistente, idMedico, hora, Fecha) > 0)
                    {
                        MessageBox.Show("Datos Agregafos Correctamente", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        CargarData(); maskedtxtHora.Clear();
                    }
                    else { MessageBox.Show("Ocurrio un Error vuelva intentarlo", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Error); maskedtxtHora.Clear(); }
                }
                else
                {
                    MessageBox.Show("Operacion cancelada", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Stop); maskedtxtHora.Clear();
                }
            }
        }

        private void btnEditarCita_Click(object sender, EventArgs e)
        {
            int idAsistente = Convert.ToInt32(cbidAsistente.Text),
                idPaciente = Convert.ToInt32(cbidPaciente.Text),
                idMedico = Convert.ToInt32(cbidMedico.Text),
                idCita = Convert.ToInt32(txtidCita.Text);
            string Fecha = DTP.Value.ToString("yyyy/MM/dd"), hora = maskedtxtHora.Text, Prescripcion = "Prescripcion";
            
            if (MessageBox.Show("Estas seguro que quieres Actualizar la Cita", "Mensaje", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                if (web.ActualizarCita(idPaciente, idAsistente, idMedico, hora, Fecha, Prescripcion, idCita) > 0)
                {
                    MessageBox.Show("Datos Actualizados Correctamente", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    CargarData(); comoboboxreseto();
                    txtidCita.Visible = false; lblidCita.Visible = false; btnEditarCita.Visible = false; maskedtxtHora.Clear(); btnAgndarCita.Visible = true;

                }
                else
                {
                    MessageBox.Show("Ocurrio un Error vuelva intentarlo", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtidCita.Visible = false; lblidCita.Visible = false; btnEditarCita.Visible = false; maskedtxtHora.Clear(); btnAgndarCita.Visible = true;
                    comoboboxreseto();
                }
            }
            else
            {
                MessageBox.Show("Operacion cancelada", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                txtidCita.Visible = false; lblidCita.Visible = false; btnEditarCita.Visible = false; maskedtxtHora.Clear(); btnAgndarCita.Visible = true;
                comoboboxreseto();
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            if(cbFiltro.Text == "Paciente Apellidos")
            {
                DataSet ds = web.DataCitasPacienteApellido(txtBusqueda.Text); cbFiltro.SelectedIndex = -1;
                dgvCitas.DataSource = ds.Tables[0];
            }
            else if(cbFiltro.Text == "Medico Apellidos")
            {
                DataSet ds = web.DataCitasMedicoApellido(txtBusqueda.Text); cbFiltro.SelectedIndex = - 1;
                dgvCitas.DataSource = ds.Tables[0];
            }
            else { MessageBox.Show("Error no se encontro nada \n por favor selecione un filtro", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Error); }
        }

        private void btnReiniciar_Click(object sender, EventArgs e)
        {
            CargarData(); cbFiltro.SelectedIndex = -1;
        }

        private void dgvCitas_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int idCita;
            int Poc = dgvCitas.CurrentRow.Index;
            if (dgvCitas.Columns[e.ColumnIndex].Name == "Column1")
            {
                idCita = Convert.ToInt32(dgvCitas[2, Poc].Value);
                if (MessageBox.Show("Estas seguro que quieres Eliminar esta Cita", "Mensaje", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    if (web.EliminarCita(idCita) > 0 )
                    {
                        MessageBox.Show("Se ha Eliminado Correctamente", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        CargarData();
                    }
                    else { MessageBox.Show("Ocurrio un Error vuelva intentarlo", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Error); }

                }
                else
                {
                    MessageBox.Show("Operacion cancelada", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }       
            }
            else if(dgvCitas.Columns[e.ColumnIndex].Name == "Column2")
            {
                MessageBox.Show("A continuacion haga la modificacion Correcpondiente ", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Information);
                idCita = Convert.ToInt32(dgvCitas[2, Poc].Value); txtidCita.Text = idCita.ToString();
                DTP.Value = Convert.ToDateTime(dgvCitas[6, Poc].Value);
                maskedtxtHora.Text = Convert.ToString(dgvCitas[7, Poc].Value);
                txtidCita.Visible = true; lblidCita.Visible = true; btnEditarCita.Visible = true; btnAgndarCita.Visible = false;
                comoboboxreseto();
            }
        }

        public void CargarData()
        {
            DataSet ds = web.DataCitas();
            dgvCitas.DataSource = ds.Tables[0];
        }

        public void ComboBoxPaciente()
        {
            DataTable dt = web.ComboBoxPaciente().Tables[0];
            cbPaciente.DataSource = dt;
            cbidPaciente.DataSource = dt;
            cbPaciente.DisplayMember = "Paciente";
            cbPaciente.ValueMember = "Paciente";
            cbidPaciente.DisplayMember = "idPaciente";
            cbidPaciente.ValueMember = "idPaciente";
            
        }

        public void ComboBoxAsistente()
        {
            DataTable dt = web.ComboBoxAsistente(lblUsuario.Text).Tables[0];
            cbAsistente.DataSource = dt;
            cbidAsistente.DataSource = dt;
            cbAsistente.DisplayMember = "Asistente";
            cbAsistente.ValueMember = "Asistente";
            cbidAsistente.DisplayMember = "idAsistenteMedica";
            cbidAsistente.ValueMember = "idAsistenteMedica";
        }

        public void ComboBoxMedico()
        {
            DataTable dt = web.ComboBoxMedico().Tables[0];
            cbMedico.DataSource = dt;
            cbidMedico.DataSource = dt;
            cbMedico.DisplayMember = "Medico";
            cbMedico.ValueMember = "Medico";
            cbidMedico.DisplayMember = "idMedico";
            cbidMedico.ValueMember = "idMedico";
        }

        public void comoboboxreseto()
        {
            cbPaciente.SelectedIndex = -1;
            cbMedico.SelectedIndex = -1;
        }
    }
}
