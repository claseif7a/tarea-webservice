﻿namespace GUI
{
    partial class Inicio
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.TxtUserI = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialCheckBox1 = new MaterialSkin.Controls.MaterialCheckBox();
            this.materialRaisedButton1 = new MaterialSkin.Controls.MaterialRaisedButton();
            this.errorIS = new System.Windows.Forms.ErrorProvider(this.components);
            this.toolTipIS = new System.Windows.Forms.ToolTip(this.components);
            this.TxtPassI = new MaterialSkin.Controls.MaterialSingleLineTextField();
            ((System.ComponentModel.ISupportInitialize)(this.errorIS)).BeginInit();
            this.SuspendLayout();
            // 
            // TxtUserI
            // 
            this.TxtUserI.Depth = 0;
            this.TxtUserI.Hint = "Usuario";
            this.TxtUserI.Location = new System.Drawing.Point(13, 97);
            this.TxtUserI.MouseState = MaterialSkin.MouseState.HOVER;
            this.TxtUserI.Name = "TxtUserI";
            this.TxtUserI.PasswordChar = '\0';
            this.TxtUserI.SelectedText = "";
            this.TxtUserI.SelectionLength = 0;
            this.TxtUserI.SelectionStart = 0;
            this.TxtUserI.Size = new System.Drawing.Size(259, 23);
            this.TxtUserI.TabIndex = 0;
            this.toolTipIS.SetToolTip(this.TxtUserI, "Escribe un Usuario");
            this.TxtUserI.UseSystemPasswordChar = false;
            // 
            // materialCheckBox1
            // 
            this.materialCheckBox1.AutoSize = true;
            this.materialCheckBox1.Depth = 0;
            this.materialCheckBox1.Font = new System.Drawing.Font("Roboto", 10F);
            this.materialCheckBox1.Location = new System.Drawing.Point(13, 182);
            this.materialCheckBox1.Margin = new System.Windows.Forms.Padding(0);
            this.materialCheckBox1.MouseLocation = new System.Drawing.Point(-1, -1);
            this.materialCheckBox1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialCheckBox1.Name = "materialCheckBox1";
            this.materialCheckBox1.Ripple = true;
            this.materialCheckBox1.Size = new System.Drawing.Size(86, 30);
            this.materialCheckBox1.TabIndex = 2;
            this.materialCheckBox1.Text = "Recordar";
            this.materialCheckBox1.UseVisualStyleBackColor = true;
            // 
            // materialRaisedButton1
            // 
            this.materialRaisedButton1.Depth = 0;
            this.materialRaisedButton1.Location = new System.Drawing.Point(184, 223);
            this.materialRaisedButton1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialRaisedButton1.Name = "materialRaisedButton1";
            this.materialRaisedButton1.Primary = true;
            this.materialRaisedButton1.Size = new System.Drawing.Size(88, 33);
            this.materialRaisedButton1.TabIndex = 3;
            this.materialRaisedButton1.Text = "Entrar";
            this.materialRaisedButton1.UseVisualStyleBackColor = true;
            this.materialRaisedButton1.Click += new System.EventHandler(this.materialRaisedButton1_Click);
            // 
            // errorIS
            // 
            this.errorIS.ContainerControl = this;
            // 
            // toolTipIS
            // 
            this.toolTipIS.IsBalloon = true;
            // 
            // TxtPassI
            // 
            this.TxtPassI.Depth = 0;
            this.TxtPassI.Hint = "Contraseña";
            this.TxtPassI.Location = new System.Drawing.Point(13, 144);
            this.TxtPassI.MouseState = MaterialSkin.MouseState.HOVER;
            this.TxtPassI.Name = "TxtPassI";
            this.TxtPassI.PasswordChar = '*';
            this.TxtPassI.SelectedText = "";
            this.TxtPassI.SelectionLength = 0;
            this.TxtPassI.SelectionStart = 0;
            this.TxtPassI.Size = new System.Drawing.Size(259, 23);
            this.TxtPassI.TabIndex = 1;
            this.toolTipIS.SetToolTip(this.TxtPassI, "Escribe un Usuario");
            this.TxtPassI.UseSystemPasswordChar = false;
            // 
            // Inicio
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(289, 268);
            this.Controls.Add(this.TxtPassI);
            this.Controls.Add(this.materialRaisedButton1);
            this.Controls.Add(this.materialCheckBox1);
            this.Controls.Add(this.TxtUserI);
            this.Name = "Inicio";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Iniciar Sesión";
            ((System.ComponentModel.ISupportInitialize)(this.errorIS)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MaterialSkin.Controls.MaterialSingleLineTextField TxtUserI;
        private MaterialSkin.Controls.MaterialCheckBox materialCheckBox1;
        private MaterialSkin.Controls.MaterialRaisedButton materialRaisedButton1;
        private System.Windows.Forms.ErrorProvider errorIS;
        private System.Windows.Forms.ToolTip toolTipIS;
        private MaterialSkin.Controls.MaterialSingleLineTextField TxtPassI;
    }
}

