﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUI
{
    public partial class Inicio : MaterialSkin.Controls.MaterialForm
    {
        localhost.WebServices web = new localhost.WebServices();
        public Inicio()
        {
            InitializeComponent();
            MaterialSkin.MaterialSkinManager Skin = MaterialSkin.MaterialSkinManager.Instance;
            Skin.AddFormToManage(this);
            Skin.Theme = MaterialSkin.MaterialSkinManager.Themes.DARK;
            Skin.ColorScheme = new MaterialSkin.ColorScheme(MaterialSkin.Primary.Green600, MaterialSkin.Primary.BlueGrey900, MaterialSkin.Primary.BlueGrey500, MaterialSkin.Accent.Orange700, MaterialSkin.TextShade.BLACK );
        }

        private void materialRaisedButton1_Click(object sender, EventArgs e)
        {
            try
            {
                if (validar())
                {
                    if (web.ConsultarUsuarioIIContraseña(TxtUserI.Text,TxtPassI.Text) > 0)// si lo datos puedes leer //  if (Leer.Read())
                    {   //verifica i inicia sesion con el tipo de usuario correponciente 
                        if (web.ConsultarTipoUsuario(TxtUserI.Text) == 1)//tipo de usuario 1 = Administrador
                        {   //ocultamos la form de login                Creando el objeto de la forma y enviando el paramero el nombre de usuario
                            FormAdministrador Admin = new FormAdministrador(TxtUserI.Text);
                            MessageBox.Show("Bienvenido " + TxtUserI.Text, " ", MessageBoxButtons.OK, MessageBoxIcon.Information);  //Mensaje de Bienvenida
                            Admin.ShowDialog(); Lmpiar();                                   //llama a la forma con el objeti creado  
                        }
                        else if (web.ConsultarTipoUsuario(TxtUserI.Text) == 2)//tipo de usuario 2 = Responsables
                        {
                          FormMedico Medico = new FormMedico(TxtUserI.Text);
                            MessageBox.Show("Bienvenido " + TxtUserI.Text, " ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            Medico.ShowDialog(); Lmpiar();
                        }
                        else if (web.ConsultarTipoUsuario(TxtUserI.Text) == 3)//tipo de usuario 3 = Clientes
                        {
                            FormAsistenteMedico Asistente = new FormAsistenteMedico(TxtUserI.Text);
                            MessageBox.Show("Bienvenido " + TxtUserI.Text, " ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            Asistente.ShowDialog();  Lmpiar();
                        }
                    }
                    else
                    {
                        MessageBox.Show("No se encontro el usuario " + TxtUserI.Text, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        Lmpiar();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error del sistema " + ex, "Fatal!! Error!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private bool validar()//metodo para validar campos vacios
        {
            bool no_error = true;

            if (TxtUserI.Text == string.Empty)
            {
                errorIS.Clear();
                errorIS.SetError(TxtUserI, "Ingrese su Usuario");
                no_error = false;
            }
            else if (TxtPassI.Text == string.Empty)
            {
                errorIS.Clear();
                errorIS.SetError(TxtPassI, "Ingrese su contraseña");
                no_error = false;
            }
            return no_error;
        }
        public void Lmpiar()
        {
            TxtUserI.Clear();
            TxtPassI.Clear();
        }
    }
}
