﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Windows.Forms;

namespace WebservicesMedical
{
    public class Codigos
    {
        public SqlConnection conn = Enlace.Conectando();
        //Datos


        public string CargarPaciente() //Para ver la info en el DataGriw
        {
            conn.Open();DataTable DT = new DataTable(); DataSet DS = new DataSet();
            SqlDataAdapter SDA = new SqlDataAdapter(
                string.Format("SELECT * FROM  Paciente"), conn); //Consilta todos los datos de la tabla
            SDA.Fill(DS);//Los datos de la consulta se almacenan en el DataTable
            DT = DS.Tables[0];
            //DGV.DataSource = DT;// Aqui es para decir que se va a mostar los datos en el DataGrid
            string registro = "";
            for (int iCampo = 0; iCampo < DT.Rows.Count; iCampo++)
            {
                Array a = DT.Rows[iCampo].ItemArray;
                for (int indice = 0; indice < a.Length; indice++)
                {
                    registro += a.GetValue(indice).ToString() + ",";
                }
                registro += "/";
            }
            return registro;
        }

        public string CargarAsistente() //Para ver la info en el DataGriw
        {
            conn.Open(); DataTable DT = new DataTable(); DataSet DS = new DataSet();
            SqlDataAdapter SDA = new SqlDataAdapter(
                string.Format("SELECT * FROM  AsistenteMedica"), conn); //Consilta todos los datos de la tabla
            SDA.Fill(DS);//Los datos de la consulta se almacenan en el DataTable
            DT = DS.Tables[0];
            //DGV.DataSource = DT;// Aqui es para decir que se va a mostar los datos en el DataGrid
            string registro = "";
            for (int iCampo = 0; iCampo < DT.Rows.Count; iCampo++)
            {
                Array a = DT.Rows[iCampo].ItemArray;
                for (int indice = 0; indice < a.Length; indice++)
                {
                    registro += a.GetValue(indice).ToString() + ",";
                }
                registro += "/";
            }
            return registro;
        }

        public string CargarMedico() //Para ver la info en el DataGriw
        {
            conn.Open(); DataTable DT = new DataTable(); DataSet DS = new DataSet();
            SqlDataAdapter SDA = new SqlDataAdapter(
                string.Format("SELECT * FROM  Medico"), conn); //Consilta todos los datos de la tabla
            SDA.Fill(DS);//Los datos de la consulta se almacenan en el DataTable
            DT = DS.Tables[0];
            //DGV.DataSource = DT;// Aqui es para decir que se va a mostar los datos en el DataGrid
            string registro = "";
            for (int iCampo = 0; iCampo < DT.Rows.Count; iCampo++)
            {
                Array a = DT.Rows[iCampo].ItemArray;
                for (int indice = 0; indice < a.Length; indice++)
                {
                    registro += a.GetValue(indice).ToString() + ",";
                }
                registro += "/";
            }
            return registro;
        }

        public string CargarCita() //Para ver la info en el DataGriw
        {
            conn.Open(); DataTable DT = new DataTable(); DataSet DS = new DataSet();
            SqlDataAdapter SDA = new SqlDataAdapter(
                string.Format("SELECT * FROM  Cita"), conn); //Consilta todos los datos de la tabla
            SDA.Fill(DS);//Los datos de la consulta se almacenan en el DataTable
            DT = DS.Tables[0];
            //DGV.DataSource = DT;// Aqui es para decir que se va a mostar los datos en el DataGrid
            string registro = "";
            for (int iCampo = 0; iCampo < DT.Rows.Count; iCampo++)
            {
                Array a = DT.Rows[iCampo].ItemArray;
                for (int indice = 0; indice < a.Length; indice++)
                {
                    registro += a.GetValue(indice).ToString() + ",";
                }
                registro += "/";
            }
            return registro;
        }

        public string CargarTipoUsuario() //Para ver la info en el DataGriw
        {
            conn.Open(); DataTable DT = new DataTable(); DataSet DS = new DataSet();
            SqlDataAdapter SDA = new SqlDataAdapter(
                string.Format("SELECT * FROM  TipoUsuario"), conn); //Consilta todos los datos de la tabla
            SDA.Fill(DS);//Los datos de la consulta se almacenan en el DataTable
            DT = DS.Tables[0];
            //DGV.DataSource = DT;// Aqui es para decir que se va a mostar los datos en el DataGrid
            string registro = "";
            for (int iCampo = 0; iCampo < DT.Rows.Count; iCampo++)
            {
                Array a = DT.Rows[iCampo].ItemArray;
                for (int indice = 0; indice < a.Length; indice++)
                {
                    registro += a.GetValue(indice).ToString() + ",";
                }
                registro += "/";
            }
            return registro;
        }

        public string CargarUsuario() //Para ver la info en el DataGriw
        {
            conn.Open(); DataTable DT = new DataTable(); DataSet DS = new DataSet();
            SqlDataAdapter SDA = new SqlDataAdapter(
                string.Format("SELECT * FROM  Usuario"), conn); //Consilta todos los datos de la tabla
            SDA.Fill(DS);//Los datos de la consulta se almacenan en el DataTable
            DT = DS.Tables[0];
            //DGV.DataSource = DT;// Aqui es para decir que se va a mostar los datos en el DataGrid
            string registro = "";
            for (int iCampo = 0; iCampo < DT.Rows.Count; iCampo++)
            {
                Array a = DT.Rows[iCampo].ItemArray;
                for (int indice = 0; indice < a.Length; indice++)
                {
                    registro += a.GetValue(indice).ToString() + ",";
                }
                registro += "/";
            }
            return registro;
        }

        //inicio de sesion
        public int ConsultarTP(string User) //Consultar el tipo de usuario
        {
            int Tp = 0; //Variable que retorna
            conn.Open();
            SqlCommand Coman = new SqlCommand(string.Format("SELECT tipoUsuarioFK FROM Usuario Where usuarioNombre = '{0}'", User), conn);
            SqlDataReader Leer = Coman.ExecuteReader(); //La consulta obtenida

            while (Leer.Read()) //Si la consulta se lee
            {
                Tp = Convert.ToInt32(Leer["tipoUsuarioFK"]); //Tp almacena el valor de la consulta al capo solicitado
            }
            conn.Close(); //cierra coneccion
            return Tp; //Retorna el valor obtenido
        }

        public int UsuarioIIcontraseña(string usuario, string contraseña)
        {
            int Res = 0;
            conn.Open();
            SqlCommand Coman = new SqlCommand("Select * From Usuario Where usuarioNombre ='" + usuario + "' and contra ='" + contraseña + "' ", conn);
            SqlDataReader Leer = Coman.ExecuteReader(); //Ya que la consulta realizada los datos los almacena una la avariable ler
            while (Leer.Read()) //Si la consulta Lee un dato significa que ya existe
            {
                Res++; //Si existe aqui aumentara la variable +1 
            }
            conn.Close();
            return Res;
        }

        //Insertar Datos1
        public int AgregarCita(int Paciente, int Asistente, int Medico, string Hora, string Fecha) 
        {
            int res = 0;  string Prescripcion = "Prescripción";
            conn.Open();        //Insertando datos en la tabla y campos correspondientes
            SqlCommand Comando = new SqlCommand(string.Format("INSERT INTO Cita VALUES ('{0}','{1}','{2}','{3}','{4}','{5}')", Paciente, Asistente, Medico, Hora, Fecha, Prescripcion ), conn);
            res = Comando.ExecuteNonQuery(); //Es para ver si se registro todos los campos necesarios
            conn.Close();
            return res;
        }

        public int AgregarPaciente(int nss, string Nombres, string ApePaterno, string ApeMaterno) 
        {
            int res = 0;
            conn.Open();        //Insertando datos en la tabla y campos correspondientes
            SqlCommand Comando = new SqlCommand(string.Format("Insert Into Paciente Values ('{0}','{1}','{2}','{3}')", nss, Nombres, ApePaterno, ApeMaterno), conn);
            res = Comando.ExecuteNonQuery(); //Es para ver si se registro todos los campos necesarios
            conn.Close();
            return res;
        }

        public int AgregarAsistente(int dni, string Nombres, string ApePaterno, string ApeMaterno)
        {
            int res = 0;
            conn.Open();        //Insertando datos en la tabla y campos correspondientes
            SqlCommand Comando = new SqlCommand
                (string.Format("Insert Into AsistenteMedica (dniAsistenteMedica,nombresAsistente,apePatAsistente,apeMatAsistente) Values ('{0}','{1}','{2}','{3}')", dni, Nombres, ApePaterno, ApeMaterno), conn);
            res = Comando.ExecuteNonQuery(); //Es para ver si se registro todos los campos necesarios
            conn.Close();
            return res;
        }

        public int AgregarMedico(int dni, string Nombres, string ApePaterno, string ApeMaterno)
        {
            int res = 0;
            conn.Open();        //Insertando datos en la tabla y campos correspondientes
            SqlCommand Comando = new SqlCommand
                (string.Format("Insert Into Medico(dniMedico,nombresMedico,apePatMedico,apeMatMedico) Values ('{0}','{1}','{2}','{3}')", dni, Nombres, ApePaterno, ApeMaterno), conn);
            res = Comando.ExecuteNonQuery(); //Es para ver si se registro todos los campos necesarios
            conn.Close();
            return res;
        }

        public int AgregarTipoUsuario(string Descripcion)
        {
            int res = 0;
            conn.Open();        //Insertando datos en la tabla y campos correspondientes
            SqlCommand Comando = new SqlCommand(string.Format("Insert Into TipoUsuario Values ('{0}'", Descripcion), conn);
            res = Comando.ExecuteNonQuery(); //Es para ver si se registro todos los campos necesarios
            conn.Close();
            return res;
        }

        public int AgregarUsuario(string Usuario, string Contra, int TipoUsuario)
        {
            int res = 0;
            conn.Open();        //Insertando datos en la tabla y campos correspondientes
            SqlCommand Comando = new SqlCommand (string.Format("Insert Into Usuario Values ('{0}','{1}','{2}')", Usuario, Contra, TipoUsuario), conn);
            res = Comando.ExecuteNonQuery(); //Es para ver si se registro todos los campos necesarios
            conn.Close();
            return res;
        }

        //Actualizar

        public int ActualizarCita(int Paciente, int Asistente, int Medico, string Hora, string Fecha, string Prescripcion, int id)
        {
            int res = 0;
            conn.Open();        //Insertando datos en la tabla y campos correspondientes
            SqlCommand Comando = new SqlCommand
                (string.Format("UPDATE Cita SET idPacienteFK = '{0}', idAsistenteMedicaFK = '{1}', idMedicoFK = '{2}', hora = '{3}', fecha = '{4}', Prescripcion = '{5}' WHERE idCita = '{6}'", Paciente, Asistente, Medico, Hora, Fecha, Prescripcion, id), conn);
            res = Comando.ExecuteNonQuery(); //Es para ver si se registro todos los campos necesarios
            conn.Close();
            return res;
        }

        public int ActualizarPaciente(int nss, string Nombres, string ApePaterno, string ApeMaterno, int id)
        {
            int res = 0;
            conn.Open();        //Insertando datos en la tabla y campos correspondientes
            SqlCommand Comando = new SqlCommand
                (string.Format("UPDATE Paciente SET numSeguroSocial = {0}, nombresPaciente = '{1}', apePatPaciente = '{2}', apeMatPaciente = '{3}' WHERE idPaciente = {4}", nss, Nombres, ApePaterno, ApeMaterno, id), conn);
            res = Comando.ExecuteNonQuery(); //Es para ver si se registro todos los campos necesarios
            conn.Close();
            return res;
        }

        public int ActualizarAsistente(int dni, string Nombres, string ApePaterno, string ApeMaterno,int usuario, int id)
        {
            int res = 0;
            conn.Open();        //Insertando datos en la tabla y campos correspondientes
            SqlCommand Comando = new SqlCommand
                (string.Format
                ("UPDATE AsistenteMedica SET dniAsistenteMedica = '{0}', nombresAsistente ='{1}', apePatAsistente = '{2}', apeMatAsistente ='{3}', usuario = '{4}' WHERE idAsistenteMedica = {5}", dni, Nombres, ApePaterno, ApeMaterno, usuario, id), conn);
            res = Comando.ExecuteNonQuery(); //Es para ver si se registro todos los campos necesarios
            conn.Close();
            return res;
        }

        public int ActualizarMedico(int dni, string Nombres, string ApePaterno, string ApeMaterno, int usuario, int id)
        {
            int res = 0;
            conn.Open();        //Insertando datos en la tabla y campos correspondientes
            SqlCommand Comando = new SqlCommand
                (string.Format("UPDATE Medico SET dniMedico = '{0}', nombresMedico = '{1}', apePatMedico = '{2}', apeMatMedico = '{3}', usuario = '{4}' WHERE idMedico = '{5}'", dni, Nombres, ApePaterno, ApeMaterno, usuario, id), conn);
            res = Comando.ExecuteNonQuery(); //Es para ver si se registro todos los campos necesarios
            conn.Close();
            return res;
        }

        public int ActualizarTipoUsuario(string Descripcion, int id)
        {
            int res = 0;
            conn.Open();        //Insertando datos en la tabla y campos correspondientes
            SqlCommand Comando = new SqlCommand
                (string.Format("UPDATE TipoUsuario SET Descripcion = '{0}' WHERE idTipoUsuario = '{1}'", Descripcion, id), conn);
            res = Comando.ExecuteNonQuery(); //Es para ver si se registro todos los campos necesarios
            conn.Close();
            return res;
        }

        public int ActualizarUsuario(string Usuario, string Contra, int TipoUsuario, int id)
        {
            int res = 0;
            conn.Open();        //Insertando datos en la tabla y campos correspondientes
            SqlCommand Comando = new SqlCommand
                (string.Format("UPDATE Usuario SET usuarioNombre = '{0}', contra = '{1}', tipoUsuarioFK = '{2}', WHERE idUsuario = '{3}'", Usuario, Contra, TipoUsuario, id), conn);
            res = Comando.ExecuteNonQuery(); //Es para ver si se registro todos los campos necesarios
            conn.Close();
            return res;
        }

        //Eliminar
        public int EliminarCita( int id)
        {
            int res = 0;
            conn.Open();        //Insertando datos en la tabla y campos correspondientes
            SqlCommand Comando = new SqlCommand
                (string.Format("DELETE Cita where idCita = '{0}'", id), conn);
            res = Comando.ExecuteNonQuery(); //Es para ver si se registro todos los campos necesarios
            conn.Close();
            return res;
        }

        public int EliminarPaciente(int id)
        {
            int res = 0;
            conn.Open();        //Insertando datos en la tabla y campos correspondientes
            SqlCommand Comando = new SqlCommand
                (string.Format("DELETE Paciente  WHERE idPaciente = '{0}'",id), conn);
            res = Comando.ExecuteNonQuery(); //Es para ver si se registro todos los campos necesarios
            conn.Close();
            return res;
        }

        public int EliminarAsistente(int id)
        {
            int res = 0;
            conn.Open();        //Insertando datos en la tabla y campos correspondientes
            SqlCommand Comando = new SqlCommand
                (string.Format
                ("DELETE AsistenteMedica WHERE idAsistenteMedica = {0}",id), conn);
            res = Comando.ExecuteNonQuery(); //Es para ver si se registro todos los campos necesarios
            conn.Close();
            return res;
        }

        public int EliminarMedico(int id)
        {
            int res = 0;
            conn.Open();        //Insertando datos en la tabla y campos correspondientes
            SqlCommand Comando = new SqlCommand
                (string.Format("DELETE Medico WHERE idMedico = '{5}'",id), conn);
            res = Comando.ExecuteNonQuery(); //Es para ver si se registro todos los campos necesarios
            conn.Close();
            return res;
        }

        public int EliminarTipoUsuario(int id)
        {
            int res = 0;
            conn.Open();        //Insertando datos en la tabla y campos correspondientes
            SqlCommand Comando = new SqlCommand
                (string.Format("DELETE TipoUsuario WHERE idTipoUsuario = '{0}'",id), conn);
            res = Comando.ExecuteNonQuery(); //Es para ver si se registro todos los campos necesarios
            conn.Close();
            return res;
        }

        public int EliminarUsuario(int id)
        {
            int res = 0;
            conn.Open();        //Insertando datos en la tabla y campos correspondientes
            SqlCommand Comando = new SqlCommand
                (string.Format("DELETE Usuario WHERE idUsuario = '{0}'",id), conn);
            res = Comando.ExecuteNonQuery(); //Es para ver si se registro todos los campos necesarios
            conn.Close();
            return res;
        }

        // Busuedas 
        public DataSet SelecionarPaciente(int idpaciente)
        {
            conn.Open();
            SqlDataAdapter sda = new SqlDataAdapter(string.Format(
                "SELECT * FROM Paciente WHERE idPaciente = {0}; ", idpaciente), conn);
            DataSet ds = new DataSet();
            sda.Fill(ds);
            return ds;
        }

        public DataSet DataCitas()
        {
            conn.Open();
            SqlDataAdapter sda = new SqlDataAdapter(string.Format(
                "SELECT  idCita, (P.nombresPaciente + ' ' + P.apePatPaciente + ' ' + P.apeMatPaciente) AS Paciente, " +
                        "(AM.nombresAsistente + ' ' + AM.apePatAsistente + ' ' + AM.apeMatAsistente) AS Asistente, "+
                        "(M.nombresMedico + ' ' + M.apePatMedico + ' ' + M.apeMatMedico) AS Medico, C.fecha, C.hora "+
                "From Cita AS C JOIN Paciente AS P "+
                "ON C.idPacienteFK = P.idPaciente JOIN AsistenteMedica AS AM "+
                "ON C.idAsistenteMedicaFK = AM.idAsistenteMedica JOIN Medico AS M "+
                "ON C.idMedicoFK = M.idMedico"), conn);
            DataSet ds = new DataSet();
            sda.Fill(ds);
            return ds;
        }

        public DataSet DataCitasPacienteApellidos(string apellido) //Busqueda personaalizada
        {
            conn.Open();
            SqlDataAdapter sda = new SqlDataAdapter(string.Format(
                "SELECT  idCita, (P.nombresPaciente + ' ' + P.apePatPaciente + ' ' + P.apeMatPaciente) AS Paciente, " +
                        "(AM.nombresAsistente + ' ' + AM.apePatAsistente + ' ' + AM.apeMatAsistente) AS Asistente, " +
                        "(M.nombresMedico + ' ' + M.apePatMedico + ' ' + M.apeMatMedico) AS Medico, C.fecha, C.hora " +
                "From Cita AS C JOIN Paciente AS P " +
                "ON C.idPacienteFK = P.idPaciente JOIN AsistenteMedica AS AM " +
                "ON C.idAsistenteMedicaFK = AM.idAsistenteMedica JOIN Medico AS M " +
                "ON C.idMedicoFK = M.idMedico " +
                "WHERE P.apePatPaciente LIKE '%{0}' ", apellido), conn);
            DataSet ds = new DataSet();
            sda.Fill(ds);
            return ds;
        }

        public DataSet DataCitasMedicoApellidos(string apellido) //Busqueda personaalizada
        {
            conn.Open();
            SqlDataAdapter sda = new SqlDataAdapter(string.Format(
                "SELECT  idCita, (P.nombresPaciente + ' ' + P.apePatPaciente + ' ' + P.apeMatPaciente) AS Paciente, " +
                        "(AM.nombresAsistente + ' ' + AM.apePatAsistente + ' ' + AM.apeMatAsistente) AS Asistente, " +
                        "(M.nombresMedico + ' ' + M.apePatMedico + ' ' + M.apeMatMedico) AS Medico, C.fecha, C.hora " +
                "From Cita AS C JOIN Paciente AS P " +
                "ON C.idPacienteFK = P.idPaciente JOIN AsistenteMedica AS AM " +
                "ON C.idAsistenteMedicaFK = AM.idAsistenteMedica JOIN Medico AS M " +
                "ON C.idMedicoFK = M.idMedico " +
                "WHERE M.apePatMedico LIKE '%{0}' ", apellido), conn);
            DataSet ds = new DataSet();
            sda.Fill(ds);
            return ds;
        }
        //LLenado de Combobox
        public DataSet ComboBoxPaciente()
        {
            conn.Open();
            DataSet ds = new DataSet();
            SqlDataAdapter sda = new SqlDataAdapter(string.Format(
                "SELECT idPaciente, (nombresPaciente +' '+ apePatPaciente +' '+ apeMatPaciente) AS Paciente FROM Paciente ORDER BY nombresPaciente ASC ;"), conn);
            sda.Fill(ds);
            return ds;  
        }

        public DataSet ComboBoxAsistente(string usuario)
        {
            conn.Open();
            DataSet ds = new DataSet();
            SqlDataAdapter sda = new SqlDataAdapter(string.Format(
                "SELECT AM.idAsistenteMedica, (AM.nombresAsistente +' '+ AM.apePatAsistente +' '+ AM.apeMatAsistente) AS Asistente " +
                "FROM Usuario AS U INNER JOIN AsistenteMedica AS AM " +
                "ON U.idUsuario = AM.usuario " +
                "WHERE U.usuarioNombre = '{0}' ;",usuario), conn);
            sda.Fill(ds);
            return ds;
        }

        public DataSet ComboBoxMedico()
        {
            conn.Open();
            DataSet ds = new DataSet();
            SqlDataAdapter sda = new SqlDataAdapter(string.Format(
                "SELECT idMedico, (nombresMedico + ' ' + apePatMedico + ' ' + apeMatMedico) AS Medico FROM Medico ORDER BY nombresMedico ASC ;"), conn);
            sda.Fill(ds);
            return ds;
        }
    }
}
/*
 *          string cadena = "Select ID_Usuario as ID From Usuarios Where NombreUsuario = '" + LblCliente.Text + "'";
            SqlCommand comando = new SqlCommand(cadena, Conn);
            SqlDataReader registro = comando.ExecuteReader();
            if (registro.Read())
            {
                TxtID.Text = registro["ID"].ToString();
                registro.Close();
            }
 *
 *
 *          int res = 0;
            SqlConnection Conn = Enlace.Conectando();
            SqlCommand comando = new SqlCommand("SELECT (nombresPaciente +' '+ apePatPaciente +' '+ apeMatPaciente)		AS Paciente ROM Paciente", conn);
            res = comando.ExecuteNonQuery();
            SqlDataAdapter da = new SqlDataAdapter(comando);
            DataTable dt = new DataTable();
            da.Fill(dt);
            Filtrar.DataSource = dt;
            Filtrar.DisplayMember = "Paciente";
            Filtrar.ValueMember = "idPaciente";
            Filtrar.SelectedValue = "idPaciente";
            Conn.Close();
            return res;
 */
