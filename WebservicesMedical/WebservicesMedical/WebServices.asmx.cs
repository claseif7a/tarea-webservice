﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Windows.Forms;

namespace WebservicesMedical
{
    /// <summary>
    /// Descripción breve de WebServices
    /// </summary>
    [WebService(Namespace = "http://WebServiceMedical.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    // [System.Web.Script.Services.ScriptService]
    public class WebServices : System.Web.Services.WebService
    {
        //Coneccion a los metodos
        Codigos Codigos = new Codigos();

        public ComboBox filtrar { get; private set; }

        [WebMethod]
        public string HelloWorld()
        {
            return "Hola a todos";
        }

        [WebMethod]
        public string HelloWorldssssssssss()
        {
            return "Hola a todosssssssss";
        }

        [WebMethod]
        public string DatosPacientes()
        {
            return Codigos.CargarPaciente();
        }

        [WebMethod]
        public string DatosAsistente()
        {
            return Codigos.CargarAsistente();
        }

        [WebMethod]
        public string DatosMedico()
        {
            return Codigos.CargarMedico();
        }

        [WebMethod]
        public string DatosCita()
        {
            return Codigos.CargarCita();
        }

        [WebMethod]
        public string DatosTipoUsuario()
        {
            return Codigos.CargarTipoUsuario();
        }

        [WebMethod]
        public string DatosUsuario()
        {
            return Codigos.CargarUsuario();
        }

        [WebMethod]
        public int ConsultarTipoUsuario(string usuario )
        {
            return Codigos.ConsultarTP(usuario);
        }

        [WebMethod]
        public int ConsultarUsuarioIIContraseña(string usuario, string contraseña)
        {
            return Codigos.UsuarioIIcontraseña(usuario,contraseña);
        }

        [WebMethod]
        public int IngresarCita(int Paciente, int Asistente, int Medico, string Hora, string Fecha)
        {
            return Codigos.AgregarCita(Paciente,Asistente,Medico,Hora,Fecha);
        }

        [WebMethod]
        public int AgregarPaciente(int nss, string Nombres, string ApePaterno, string ApeMaterno)
        {
            return Codigos.AgregarPaciente(nss,Nombres,ApePaterno,ApeMaterno);
        }

        [WebMethod]
        public int AgregarAsistente(int dni, string Nombres, string ApePaterno, string ApeMaterno)
        {
            return Codigos.AgregarAsistente(dni, Nombres, ApePaterno, ApeMaterno);
        }

        [WebMethod]
        public int AgregarMedico(int dni, string Nombres, string ApePaterno, string ApeMaterno)
        {
            return Codigos.AgregarMedico(dni, Nombres, ApePaterno, ApeMaterno);
        }

        [WebMethod]
        public int AgregarTipoUsuario(string Descripcion)
        {
            return Codigos.AgregarTipoUsuario(Descripcion);
        }

        [WebMethod]
        public int AgregarUsuario(string Usuario, string Contra, int TipoUsuario)
        {
            return Codigos.AgregarUsuario(Usuario,Contra,TipoUsuario);
        }

        [WebMethod]
        public int ActualizarCita(int Paciente, int Asistente, int Medico, string Hora, string Fecha, string Prescripcion, int id)
        {
            return Codigos.ActualizarCita(Paciente,Asistente,Medico,Hora,Fecha,Prescripcion,id);
        }

        [WebMethod]
        public int ActualizarPaciente(int nss, string Nombres, string ApePaterno, string ApeMaterno, int id)
        {
            return Codigos.ActualizarPaciente(nss,Nombres,ApePaterno,ApeMaterno,id);
        }

        [WebMethod]
        public int ActualizarAsistente(int dni, string Nombres, string ApePaterno, string ApeMaterno, int usuario, int id)
        {
            return Codigos.ActualizarAsistente(dni,Nombres,ApePaterno,ApeMaterno,usuario,id);
        }

        [WebMethod]
        public int ActualizarMedico(int dni, string Nombres, string ApePaterno, string ApeMaterno, int usuario, int id)
        {
            return Codigos.ActualizarMedico(dni,Nombres,ApePaterno,ApeMaterno,usuario,id);
        }

        [WebMethod]
        public int ActualizarTipoUsuario(string Descripcion, int id)
        {
            return Codigos.ActualizarTipoUsuario(Descripcion,id);
        }

        [WebMethod]
        public int ActualizarUsuario(string Usuario, string Contra, int TipoUsuario, int id)
        {
            return Codigos.ActualizarUsuario(Usuario,Contra,TipoUsuario,id);
        }

        [WebMethod]
        public DataSet SelecionarPaciente(int idpaciente)
        {
            return Codigos.SelecionarPaciente(idpaciente);
        }

        [WebMethod]
        public DataSet DataCitas()
        {
            return Codigos.DataCitas();
        }

        [WebMethod]
        public DataSet DataCitasPacienteApellido(string apellidoPaciente)
        {
            return Codigos.DataCitasPacienteApellidos(apellidoPaciente);
        }

        [WebMethod]
        public DataSet DataCitasMedicoApellido(string apellidoMedico)
        {
            return Codigos.DataCitasMedicoApellidos(apellidoMedico);
        }

        [WebMethod]
        public DataSet ComboBoxPaciente()
        {
            
            return Codigos.ComboBoxPaciente();
        }

        [WebMethod]
        public DataSet ComboBoxAsistente(string usuario)
        {

            return Codigos.ComboBoxAsistente(usuario);
        }

        [WebMethod]
        public DataSet ComboBoxMedico()
        {
            return Codigos.ComboBoxMedico();
        }

        [WebMethod]
        public int EliminarTipoUsuario(int id)
        {
            return Codigos.EliminarTipoUsuario(id);
        }

        [WebMethod]
        public int EliminarUsuario(int id)
        {
            return Codigos.EliminarUsuario(id);
        }

        [WebMethod]
        public int EliminarPaciente(int id)
        {
            return Codigos.EliminarPaciente(id);
        }

        [WebMethod]
        public int EliminarAsistente(int id)
        {
            return Codigos.EliminarAsistente(id);
        }

        [WebMethod]
        public int EliminarMedico(int id)
        {
            return Codigos.EliminarMedico(id);
        }

        [WebMethod]
        public int EliminarCita(int id)
        {
            return Codigos.EliminarCita(id);
        }
    }
}
